@servers(['local' => 'vagrant@192.168.10.10', 'web' => 'root@quickdemo.ml'])


# envoy run push --message='git message here' --migrate || --refresh
# envoy run pull-on-server
# envoy run deploy --message="git message here" --migrate --composer_update --route_cache --config_cache

###########
# STORIES #
###########
@story('deploy')
push
pull-on-server
@endstory

#########
# TASKS #
#########

###################
# PUSH TASK START #
###################

@task('push', ['on' => 'local'])

printf "$(date '+%d-%m-%Y %H-%M-%S')==========Start push command.....==========\n"

################################
# NAVIGATE TO PROJECT DIECTORY #
################################
cd /var/www/loanapp

#######################
# ADD UNTRACKED FILES #
#######################
printf "$(date '+%d-%m-%Y %H-%M-%S')==========Add untracked files....=========="
git add .

####################################################
# ADD A COMMIT MESSAGE IF PASSED IN OR USE DEFAULT #
####################################################
printf "$(date '+%d-%m-%Y %H-%M-%S')==========Start git commit.......=========="
@if ($message)
    git commit -m "{{ $message }}"
@else
    git commit -m "regular updates"
@endif

#############################################################
# RUN THE MIGRATION OR REFRESH IT BASED ON PASSED IN OPTION #
#############################################################
@if ($migrate)
    php artisan migrate
@elseif ($refresh)
    php artisan migrate:refresh
@endif

##############################################
# PUSH TO MASTER/HEAD OR BRANCH AS IT MAY BE #
##############################################
printf "$(date '+%d-%m-%Y %H-%M-%S')==========Git push to master......=========="
git push -u origin master

#print a quote
php artisan inspire

#insert an extra line
printf "\n"

@endtask
############
# END TASK #
############


#############################
# PULL ON SERVER TASK START #
#############################
@task('pull-on-server', ['on' => 'web'])
printf "$(date '+%d-%m-%Y %H-%M-%S')==========Pull on server started==========\n"

##############################
# NAVIGATE TO PROJECT FOLDER #
##############################
cd /var/www/webapp

#################################
# CLEAR CACHED ROUTES & CONFIGS #
#################################
printf "$(date '+%d-%m-%Y %H-%M-%S')==========Clear cache & routes=========="
php artisan route:clear
php artisan config:clear

###############
# DO GIT PULL #
###############
printf "$(date '+%d-%m-%Y %H-%M-%S')==========Git pull start ......=========="
sudo git stash
sudo git pull

######################
# DO COMPOSER UPDATE #
######################
printf "$(date '+%d-%m-%Y %H-%M-%S')==========Composer update start.=========="
@if( $composer_install )
    composer install
@endif

############################
# CACHE ROUTES AND CONFIGS #
############################
printf "$(date '+%d-%m-%Y %H-%M-%S')==========Cache route & configs.=========="
@if($route_cache)
    php artisan route:cache
@endif
@if($config_cache)
    php artisan config:cache
@endif

##########################
# MIGRATE PENDING TABLES #
##########################
@if ( $migrate )
    printf "$(date '+%d-%m-%Y %H-%M-%S')==========Migrate pending tables=========="
    php artisan migrate
@endif
@endtask
