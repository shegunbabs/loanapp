<?php

namespace App\Models;

use App\Models\SO\StandingOrder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['paid_at', 'transaction_date'];



    //////////////////////////////////////////////////////////
    /// RELATIONSHIPS ////////////////////////////////////////
    /////////////////////////////////////////////////////////

    public function authorization()
    {
        return $this->belongsTo(Authorization::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }


    public function standingOrder()
    {
        return $this->hasOne(StandingOrder::class);
    }



    ////////////////////////////////////////////////////////
    /// CUSTOM MUTATORS ////////////////////////////////////
    ///////////////////////////////////////////////////////

    public function setPaidAtAttribute($value)
    {
        $this->attributes['paid_at'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }


    public function setTransactionDateAttribute($value)
    {
        $this->attributes['transaction_date'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }


    public function getAmountAttribute($value)
    {
        return $value/100;
    }


    //////////////////////////////////////////////////////////////////
    /// CUSTOM METHODS ///////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    public function _create($data, $customer_id=null, $authorization_id=null)
    {
        $fields = [
            'domain' => $data->domain ?? null,
            'status' => $data->status ?? null,
            'reference' => $data->reference ?? null,
            'amount' => $data->amount ?? null,
            'message' => $data->message ?? null,
            'gateway_response' => $data->gateway_response ?? null,
            //'paid_at' => $data->paid_at,
            'channel' => $data->channel ?? null,
            'ip_address' => $data->ip_address ?? null,
            'fees' => $data->fees ?? null,
            //'fees_split' => $data->fees_split,
            'authorization_id' => $authorization_id ?? null,
            'customer_id' => $customer_id ?? null,
            'plan' => $data->plan ?? null,
            'transaction_date' => $data->transaction_date ?? null,
        ];

        !empty($data->id) ? $fields['tid'] = $data->id : null;
        !empty($data->paid_at) ? $fields['paid_at'] = $data->paid_at : null;
        !empty($data->fees_split) ? $fields['fees_split'] = $data->fees_split : null;

        return $this->updateOrCreate(
            ['reference' => $data->reference],
            $fields
        );
    }
}
