<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BvnResponse extends Model
{
    protected $guarded = ['id'];


    public function findBvn($bvn)
    {
        return $this->where('bvn', $bvn)->first();
    }
}
