<?php

namespace App\Models\Remita;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RemitaSalaryPayment extends Model
{

    protected $fillable = ['payment_date', 'amount', 'account_number', 'bank_code'];
    protected $dates = ['payment_date'];


    public function remitaData()
    {
        return $this->belongsTo(RemitaData::class);
    }


    public function setPaymentDateAttribute($value)
    {
        $this->attributes['payment_date'] = Carbon::parse($value)->format("Y-m-d H:i:s");
    }
}
