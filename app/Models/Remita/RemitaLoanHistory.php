<?php

namespace App\Models\Remita;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RemitaLoanHistory extends Model
{

    protected $guarded = ['id'];
    protected $dates = ['date_disbursed'];


    public function remitaData()
    {
        return $this->belongsTo(RemitaData::class);
    }


    /////////////////////////////////////////////////////////
    /// SET MUTATIONS ///////////////////////////////////////
    /////////////////////////////////////////////////////////

    public function setDateDisbursedAttribute($value)
    {
        $this->attributes['date_disbursed'] = Carbon::parse($value)->format("Y-m-d H:i:s");
    }
}
