<?php

namespace App\Models\Remita;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SalaryHistoryResponse extends Model
{
    protected $guarded = ['id'];


    /////////////////////////////////////////////////////////////////////////
    /// RELATIONSHIPS ///////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////
    /// SET MUTATORS ////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////

    public function setSalaryPaymentDetailsAttribute($value)
    {
        $this->attributes['salary_payment_details'] = json_encode($value);
    }


    public function setLoanHistoryDetailsAttribute($value)
    {
        $this->attributes['loan_history_details'] = json_encode($value);
    }


    public function setResponseDateAttribute($value)
    {
        $this->attributes['response_date'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }


    //////////////////////////////////////////////////////////////////////////
    /// GET MUTATORS /////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    public function getSalaryPaymentDetailsAttribute($value)
    {
        return json_decode($value, true);
    }



    public function getLoanHistoryDetailsAttribute($value)
    {
        return json_decode($value, true);
    }

}
