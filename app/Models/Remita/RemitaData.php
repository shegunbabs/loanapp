<?php

namespace App\Models\Remita;

use App\Models\Applicant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RemitaData extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['first_payment_date'];



    //////////////////////////////////////////////////////////
    /// CUSTOM METHODS ///////////////////////////////////////
    //////////////////////////////////////////////////////////



    //////////////////////////////////////////////////////////
    /// RELATIONSHIPS ////////////////////////////////////////
    //////////////////////////////////////////////////////////

    public function remitaSalaryPayments()
    {
        return $this->hasMany(RemitaSalaryPayment::class);
    }


    public function remitaLoanHistory()
    {
        return $this->hasMany(RemitaLoanHistory::class);
    }


    public function applicant()
    {
        return $this->belongsTo(Applicant::class);
    }



    /////////////////////////////////////////////////////////
    /// SET MUTATIONS ///////////////////////////////////////
    /////////////////////////////////////////////////////////

    public function setFirstPaymentDateAttribute($value)
    {
        $this->attributes['first_payment_date'] = Carbon::parse($value)->format("Y-m-d H:i:s");
    }
}
