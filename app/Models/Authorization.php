<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Authorization extends Model
{
    protected $guarded = ['id'];


    //////////////////////////////////////////////////////////
    /// RELATIONSHIPS ////////////////////////////////////////
    /////////////////////////////////////////////////////////

    public function transaction()
    {
        return $this->hasOne(Transaction::class);
    }


    //////////////////////////////////////////////////////////
    /// CUSTOM METHODS ///////////////////////////////////////
    /////////////////////////////////////////////////////////
    public function _create($data)
    {
        if (is_object($data))
            $data = (array)$data;

        return $this->updateOrCreate(
            ['authorization_code' => $data['authorization_code']],
            [
                'bin' => $data['bin'],
                'last4' => $data['last4'],
                'exp_month' => $data['exp_month'],
                'exp_year' => $data['exp_year'],
                'channel' => $data['channel'],
                'card_type' => $data['card_type'],
                'bank' => $data['bank'],
                'country_code' => $data['country_code'],
                'brand' => $data['brand'],
                'reusable' => $data['reusable'],
                'signature' => $data['signature'],
            ]);
    }
}
