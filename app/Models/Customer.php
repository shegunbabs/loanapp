<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $guarded = ['id'];


    //////////////////////////////////////////////////////////
    /// RELATIONSHIPS ////////////////////////////////////////
    /////////////////////////////////////////////////////////

    public function transaction()
    {
        return $this->hasOne(Transaction::class);
    }


    //////////////////////////////////////////////////////////
    /// CUSTOM METHODS ///////////////////////////////////////
    /////////////////////////////////////////////////////////

    public function _create($data, $email=null)
    {
        if ( is_object($data) )
            $data = (array) $data;

        return $this->updateOrCreate(
            ['pid' => $data['id']],
            [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'customer_code' => $data['customer_code'],
                'phone' => $data['phone'],
                'metadata' => $data['metadata'],
                'risk_action' => $data['risk_action'],
            ]
        );
    }
}
