<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScoringData extends Model
{

    protected $fillable = ['age', 'salary_amount', 'average_salary', 'allowable_salary_percent', 'allowable_salary_percent_amount'];



    ///////////////////////////////////////////////////////
    /// RELATIONSHIPS /////////////////////////////////////
    ///////////////////////////////////////////////////////

    public function applicant()
    {
        return $this->belongsTo(Applicant::class);
    }

}
