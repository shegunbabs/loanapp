<?php

namespace App\Models;

use App\Models\App\LoanApplication;
use App\Models\App\LoanDisbursement;
use App\Models\App\LoanRequest;
use App\Models\Remita\RemitaData;
use App\Models\Remita\SalaryHistoryResponse;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Applicant extends Authenticatable
{
    use Notifiable;

    protected $fillable = ['firstname', 'lastname', 'email', 'mobile', 'bvn', 'password'];

    protected $hidden = ['password', 'remember_token'];


    //////////////////////////////////////////////////////////
    /// CUSTOM METHODS ///////////////////////////////////////
    //////////////////////////////////////////////////////////

    public function hasRemitaData()
    {
        return $this->remitaData()->exists();
    }


    public function hasRemitaSalaryResponseData()
    {
        return SalaryHistoryResponse::where('mobile', $this->mobile)->first();
    }


    public function isNotRemitaCustomer()
    {
        if ( !$this->hasRemitaData() && $this->hasRemitaSalaryResponseData() ){
            return true;
        }
        return false;
    }




    ////////////////////////////////////////////////////////////////
    /// ATTRIBUTES /////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    public function setFirstnameAttribute($value)
    {
        $this->attributes['firstname'] = strtolower($value);
    }


    public function setLastnameAttribute($value)
    {
        $this->attributes['lastname'] = strtolower($value);
    }


    ////////////////////////////////////////////////////////////////
    /// RELATIONSHIPS //////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    public function remitaData()
    {
        return $this->hasOne(RemitaData::class);
    }


    public function loanRequests()
    {
        return $this->hasMany(LoanRequest::class);
    }


    public function loanApplications()
    {
        return $this->hasMany(LoanApplication::class);
    }


    public function loanDisbursements()
    {
        return $this->hasMany(LoanDisbursement::class);
    }


    public function scoringData()
    {
        return $this->hasOne(ScoringData::class);
    }
}
