<?php

namespace App\Models\App;

use Illuminate\Database\Eloquent\Model;

class RepaymentSchedule extends Model
{

    protected $fillable = [];
    protected $dates = [];


    /////////////////////////////////////////////
    /// RELATIONSHIPS ///////////////////////////
    /////////////////////////////////////////////


    public function loanDisbursement()
    {
        return $this->belongsTo(LoanDisbursement::class);
    }
}
