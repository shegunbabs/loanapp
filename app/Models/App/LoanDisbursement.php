<?php

namespace App\Models\App;

use App\Models\Applicant;
use Illuminate\Database\Eloquent\Model;

class LoanDisbursement extends Model
{

    protected $fillable = [];

    protected $dates = [];


    ////////////////////////////////////////////
    /// RELATIONSHIPS //////////////////////////
    ////////////////////////////////////////////


    public function applicant()
    {
        return $this->belongsTo(Applicant::class);
    }


    public function loanApplication()
    {
        return $this->belongsTo(LoanApplication::class);
    }


    public function repaymentSchedules()
    {
        return $this->hasMany(RepaymentSchedule::class);
    }
}
