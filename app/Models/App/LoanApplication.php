<?php

namespace App\Models\App;

use App\Models\Applicant;
use Illuminate\Database\Eloquent\Model;

class LoanApplication extends Model
{

    protected $fillable = ['amount', 'tenor', 'interest_rate_percent', 'monthly_amount_payable', 'total_amount_payable',
        'collection_date', 'account_number', 'bank_name', 'average_salary', 'net_salary', 'outstanding_loan_amount', 'outstanding_loan_monthly_payment'];

    protected $dates = ['collection_date'];

    const PENDING = 0;
    const PROCESSING = 1;
    const ON_HOLD = 2;
    const DISBURSED = 3;
    const REJECTED = 4;


    ////////////////////////////////////////////////
    /// RELATIONSHIPS //////////////////////////////
    ////////////////////////////////////////////////


    public function applicant()
    {
        return $this->belongsTo(Applicant::class);
    }


    public function loanRequest()
    {
        return $this->belongsTo(LoanRequest::class);
    }


    public function loanDisbursement()
    {
        return $this->hasOne(LoanDisbursement::class);
    }
}
