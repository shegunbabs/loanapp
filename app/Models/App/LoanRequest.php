<?php

namespace App\Models\App;

use App\Models\Applicant;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class LoanRequest extends Model
{

    protected $fillable = ['amount', 'tenor', 'interest_rate_percent', 'monthly_amount_payable', 'total_amount_payable',];

    const REQUEST_PENDING = 0;
    const REQUEST_ONHOLD = 1;
    const REQUEST_PROCESSING = 2;
    const REQUEST_REJECTED = 3;
    const REQUEST_PROCESSED = 4;

    CONST OFFER_PENDING = 0;
    CONST OFFER_GIVEN = 1;
    CONST OFFER_ACCEPTED = 2;
    CONST OFFER_REJECTED = 3;
    CONST OFFER_ELAPSED = 4;


    ///////////////////////////////////////////////////////
    /// RELATIONSHIPS /////////////////////////////////////
    ///////////////////////////////////////////////////////


    public function applicant()
    {
        return $this->belongsTo(Applicant::class);
    }


    public function loanApplication()
    {
        return $this->hasOne(LoanApplication::class);
    }


    ////////////////////////////////////////////////////////
    /// CUSTOM METHODS /////////////////////////////////////
    ////////////////////////////////////////////////////////

    public function hasLoanRequest()
    {
        return $this->statusPending() ?? $this->statusOnHold() ?? $this->statusProcessing() ?? $this->statusProcessed();
    }


    public function statusText()
    {
        $text = null;
        switch (true) {
            case self::REQUEST_PENDING === (int)$this->request_status:
                $text = 'Pending';
                break;
            case self::REQUEST_ONHOLD === (int)$this->request_status:
                $text = 'On Hold';
                break;
            case self::REQUEST_PROCESSING === (int)$this->request_status:
                $text = 'Processing';
                break;
            case self::REQUEST_REJECTED === (int)$this->request_status:
                $text = 'Rejected';
                break;
            case self::REQUEST_PROCESSED === (int)$this->request_status:
                $text = 'Processed';
                break;
        }
        return $text;
    }

    public function offerStatus()
    {
        $text = null;
        switch (true) {
            case self::OFFER_PENDING === (int)$this->offer_status:
                $text = 'offer pending';
                break;
            case self::OFFER_GIVEN === (int)$this->offer_status:
                $text = 'offer given';
                break;
            case self::OFFER_ACCEPTED === (int)$this->offer_status:
                $text = 'offer accepted';
                break;
            case self::OFFER_REJECTED === (int)$this->offer_status:
                $text = 'offer rejected';
                break;
            case self::OFFER_ELAPSED === (int)$this->offer_status:
                $text = 'offer elapsed';
                break;
        }
        return $text;
    }

    public function statusPending()
    {
        if ($this->request_status == self::REQUEST_PENDING && $this->offer_status == self::OFFER_PENDING)
            return TRUE;
        return FALSE;

    }

    public function statusOnHold()
    {
        if ($this->request_status == self::REQUEST_ONHOLD)
            return true;
        return false;
    }


    public function statusProcessing()
    {
        if ($this->request_status == self::REQUEST_PROCESSING)
            return true;
        return false;
    }


    public function statusRejected()
    {
        if ($this->request_status == self::REQUEST_REJECTED)
            return true;
        return false;
    }


    public function statusProcessed()
    {
        if ( $this->request_status == self::REQUEST_PROCESSED )
            return true;
        return false;
    }


    ////////////////////////////////////////////////////////
    /// CUSTOM SET METHODS /////////////////////////////////
    ////////////////////////////////////////////////////////

    public function setStatusOnHold()
    {
        if ($this->request_status == self::REQUEST_PENDING) {
            $this->request_status = self::REQUEST_ONHOLD;
            $this->offer_status = self::OFFER_PENDING;
            return $this->save();
        }
    }


    public function setStatusProcessing()
    {
        if ($this->request_status == self::REQUEST_PENDING || $this->request_status == self::REQUEST_ONHOLD) {
            $this->request_status = self::REQUEST_PROCESSING;
            $this->offer_status = self::OFFER_PENDING;
            return $this->save();
        }
    }


    public function setStatusRejected()
    {
        if ($this->request_status == self::REQUEST_PENDING || $this->request_status == self::REQUEST_ONHOLD || $this->request_status == self::REQUEST_PROCESSING) {
            $this->request_status = self::REQUEST_REJECTED;
            $this->offer_status = self::OFFER_PENDING;
            return $this->save();
        }
    }
}
