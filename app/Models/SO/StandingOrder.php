<?php

namespace App\Models\SO;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Model;

class StandingOrder extends Model
{
    protected $dates = ['start_date', 'end_date'];

    protected $guarded = [
        'id', 'activated', 'disabled', 'payment_confirmed'
    ];


    //////////////////////////////////////////////////////////////
    /// RELATIONSHIP /////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    public function standingOrderRepayment()
    {
        return $this->hasMany(StandingOrderRepayment::class);
    }


    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }


    //////////////////////////////////////////////////////////////
    /// SET MUTATOR //////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    public function setPayersNameAttribute($value)
    {
        $this->attributes['payers_name'] = strtolower($value);
    }

    public function setPayersEmailAttribute($value)
    {
        $this->attributes['payers_email'] = strtolower($value);
    }


    /////////////////////////////////////////////////////////////
    /// CUSTOM METHODS //////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    public function doStandingOrderRepaymentSchedule()
    {
        $times = (int) $this->no_of_debit;
        $transaction_date = $this->start_date;
        $payment_frequency = $this->payment_frequency;
        for ($i = 1; $i <= $times; $i++):
            $this->standingOrderRepayment()->create([
                'transaction_date' => $transaction_date,
                'transaction_amount' => $this->total_amount,
                'amount' => $this->amount,
            ]);
            $transaction_date = $payment_frequency == 'monthly' ? $transaction_date->addMonth() : $payment_frequency == 'weekly' ? $transaction_date->addWeek() : $transaction_date->addWeekday() ;
        endfor;

    }

    public function activate()
    {
        $this->activated = 1;
        return $this->save();
    }


    public function disable()
    {
        $this->disabled = 0;
        return $this->save();
    }


    public function confirmPayment()
    {
        if ($this->payment_confirmed == 0) {
            $this->payment_confirmed = 1;
            return $this->save();
        }
    }


    public function finishProcess()
    {
        $this->process_finished = 1;
        return $this->save();
    }


    public function finishGlobal()
    {
        $this->global_finished = 1;
        return $this->save();
    }


    public function setAuthReusable($value = TRUE)
    {
        isset($value) === TRUE ? $this->authorization_reusable = 1 : $this->authorization_reusable = 0;
        return $this->save();
    }


}
