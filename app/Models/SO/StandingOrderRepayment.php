<?php

namespace App\Models\SO;

use Illuminate\Database\Eloquent\Model;

class StandingOrderRepayment extends Model
{

    protected $fillable = ['transaction_date', 'transaction_amount', 'amount', 'payment_reference'];
    protected $dates = ['transaction_date', 'authorization_check_last_timestamp'];


    ////////////////////////////////////////////////////////
    /// RELATIONSHIPS //////////////////////////////////////
    ////////////////////////////////////////////////////////

    public function standingOrder()
    {
        return $this->belongsTo(StandingOrder::class);
    }


    ////////////////////////////////////////////////////////
    /// MUTATORS ///////////////////////////////////////////
    ///////////////////////////////////////////////////////

    public function getTransactionAmountAttribute($value)
    {
        return (int) $value;
    }

    public function getAmountAttribute($value)
    {
        return (int) $value;
    }

    public function getAuthorizationCheckStatusAttribute($value)
    {
        if (isset($value) === true)
            return (int) $value;
        return $value;
    }


    ////////////////////////////////////////////////////////
    /// CUSTOM METHODS /////////////////////////////////////
    ////////////////////////////////////////////////////////

    public function checkAuthorization()
    {
        $params = [
            'authorization_code' => $this->standingOrder->transaction->authorization->authorization_code,
            'amount' => $this->transaction_amount,
            'email' => $this->standingOrder->payers_email,
        ];
        $paystack = app('PayStack');
        $response = $paystack->transaction->checkAuthorization($params);

        //save checkAuthorization response to row
        $this->authorization_check_status = $response['response']->status;
        $this->authorization_check_message = $response['response']->message;
        $this->authorization_check_last_timestamp = now()->format('Y-m-d H:i:s');
        $this->save();
    }

    public function processTransaction()
    {
        $paystack = app('PayStack');
        $params = [
            'reference' => str_random(),
            'authorization_code' => $this->standingOrder->transaction->authorization->authorization_code,
            'amount' => $this->transaction_amount,
            'email' => $this->standingOrder->payers_email,
        ];
        $response = $paystack->transaction->chargeAuthorization($params);

        //save customer
        $customer = (array) $response['response']->data->customer;
        $c = $this->standingOrder->transaction->customer->_create($customer);

        //save authorization
        $authorization = $response['response']->data->authorization;
        $a = $this->standingOrder->transaction->authorization->_create($authorization);

        //save transaction
        $t = $this->standingOrder->transaction->_create($response['response']->data, $c->id, $a->id);


        $this->transaction_id = $t->id;
        $this->payment_reference = $response['response']->data->reference;
        $this->payment_confirmed = $response['response']->status ? 1 : 0;
        $this->processed = 1;
//        $this->authorization_id = $a ? $a->id : null;
        $this->save();
    }
}
