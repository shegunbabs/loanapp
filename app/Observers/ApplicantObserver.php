<?php

namespace App\Observers;

use App\Models\Applicant;

class ApplicantObserver
{

    /**
     * @param Applicant $applicant
     */
    public function creating(Applicant $applicant)
    {
        $applicant->api_token = str_random(60);
    }

    /**
     * Handle the applicant "created" event.
     *
     * @param  \App\Models\Applicant  $applicant
     * @return void
     */
    public function created(Applicant $applicant)
    {
        //
    }

    /**
     * Handle the applicant "updated" event.
     *
     * @param  \App\Models\Applicant  $applicant
     * @return void
     */
    public function updated(Applicant $applicant)
    {
        //
    }

    /**
     * Handle the applicant "deleted" event.
     *
     * @param  \App\Models\Applicant  $applicant
     * @return void
     */
    public function deleted(Applicant $applicant)
    {
        //
    }

    /**
     * Handle the applicant "restored" event.
     *
     * @param  \App\Models\Applicant  $applicant
     * @return void
     */
    public function restored(Applicant $applicant)
    {
        //
    }

    /**
     * Handle the applicant "force deleted" event.
     *
     * @param  \App\Models\Applicant  $applicant
     * @return void
     */
    public function forceDeleted(Applicant $applicant)
    {
        //
    }
}
