<?php

namespace App\Jobs;

use App\Models\App\LoanRequest;
use App\Models\Applicant;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Auth;

class CreateLoanRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $amount;
    private $tenor;
    /**
     * @var int
     */
    private $interest_rate;
    private $applicant_id;

    public $tries = 2;

    /**
     * Create a new job instance.
     *
     * @param $applicant_id
     * @param $amount
     * @param $tenor
     * @param int $interest_rate
     */
    public function __construct($applicant_id, $amount, $tenor, $interest_rate=7)
    {

        $this->applicant_id = $applicant_id;
        $this->amount = $amount;
        $this->tenor = $tenor;
        $this->interest_rate = $interest_rate;
    }

    /**
     * Execute the job.
     *
     * @param Applicant $applicant
     * @return void
     */
    public function handle(Applicant $applicant)
    {

        $monthly_repayment = round(
            emi($this->interest_rate/100, $this->tenor, $this->amount),
            2
        );

        $applicant_row = $applicant->find($this->applicant_id);

        if ( $applicant_row && $applicant_row->offer_status == 0 ){

            $applicant_row->loanRequests()->create([
                'amount' => $this->amount,
                'tenor' => $this->tenor,
                'interest_rate_percent' => $this->interest_rate,
                'monthly_amount_payable' => $monthly_repayment,
                'total_amount_payable' => $monthly_repayment * $this->tenor,
            ]);
        }

        if ( !Auth::guard('ap')->check() ){
            Auth::guard('ap')->login($applicant_row, true);
        }
    }
}
