<?php

namespace App\Jobs\SO;

use App\Library\SOPaystackTransaction;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class VerifyTransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var string
     */
    private $payment_ref;

    /**
     * Create a new job instance.
     *
     * @param string $payment_ref
     */
    public function __construct(string $payment_ref)
    {
        //
        $this->payment_ref = $payment_ref;
    }

    /**
     * Execute the job.
     *
     * @param SOPaystackTransaction $paystackTransaction
     * @return void
     */
    public function handle(SOPaystackTransaction $paystackTransaction)
    {
        $paystackTransaction->verifyTrans($this->payment_ref);
        $paystackTransaction->updateSORow();
    }
}
