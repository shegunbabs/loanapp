<?php

namespace App\Jobs\Remita;

use App\Library\SalaryHistoryWrapper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetSalaryHistory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $mobile;
    private $applicant_id;
    public $tries = 2;

    /**
     * Create a new job instance.
     *
     * @param $mobile
     * @param $applicant_id
     */
    public function __construct($mobile, $applicant_id)
    {

        $this->mobile = $mobile;
        $this->applicant_id = $applicant_id;
    }

    /**
     * Execute the job.
     *
     * @param SalaryHistoryWrapper $wrapper
     * @return void
     */
    public function handle(SalaryHistoryWrapper $wrapper)
    {
        $wrapper->query($this->mobile, $this->applicant_id);
    }
}
