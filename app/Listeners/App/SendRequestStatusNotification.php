<?php

namespace App\Listeners\App;

use App\Events\LoanRequestRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class SendRequestStatusNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoanRequestRejected  $event
     * @return void
     */
    public function handle(LoanRequestRejected $event)
    {
        $loanRequestModel = $event->loanRequestModel;
        //Log::info(serialize($loanRequestModel));
        //send notification here
        //user notification
        Notification::send($loanRequestModel->applicant()->first(), new \App\Notifications\App\LoanRequestRejected($loanRequestModel));
        //backoffice notification

    }
}
