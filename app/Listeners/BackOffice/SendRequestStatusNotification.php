<?php

namespace App\Listeners\BackOffice;

use App\Events\LoanRequestRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRequestStatusNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoanRequestRejected  $event
     * @return void
     */
    public function handle(LoanRequestRejected $event)
    {
        //
    }
}
