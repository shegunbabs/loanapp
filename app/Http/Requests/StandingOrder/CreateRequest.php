<?php

namespace App\Http\Requests\StandingOrder;

use App\Models\SO\StandingOrder;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    private $apiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'beneficiary'           => 'required',
            'purpose'               => 'required',
            'payment_type'          => 'required',
            'payment_frequency'     => 'required|in:daily,weekly,monthly',
            'amount'                => 'required',
            'start_date'            => 'required|date|after_or_equal:today',
            'no_of_debit'           => 'required|min:1|max:24',
            'end_date'              => 'required|date|after:today',
            'payers_name'           => 'required',
            'payers_phone'          => 'required',
            'payers_email'          => ['required', 'email', 'confirmed',
//                Rule::unique('standing_orders', 'payers_email')->where(function($query){
//                    return $query->where('finished', 0);})
            ],
            'funding_source'        => 'required',
            'total_amount'          => 'required',
        ];
    }


    public function save()
    {
        return $this->process();
    }


    private function process()
    {
        $payment_ref = str_random();

        //save to standing_orders table
        $data = $this->except('payers_email_confirmation');
        $data['start_date'] = Carbon::parse($this->get('start_date'));
        $data['end_date'] = Carbon::parse($this->get('end_date'));
        $data['payment_reference'] = $payment_ref;

        $model = $this->saveToTable($data);

        $params = [
            'callback_url' => route('so.verify.reference'),
            'reference' => $payment_ref,
            'amount' =>  (50 + (50*.015)) * 100,
            'email' => $this->get('payers_email'),
            'channels' => [$this->get('funding_source')],
        ];

        //call payment processor
        $paystack = app('PayStack');
        $this->apiResponse = $paystack->transaction->initialize($params);

        return ['api_response' => $this->apiResponse];
    }


    private function saveToTable($array)
    {
        return StandingOrder::create($array);
    }


    public function requestPassed()
    {
        if ($this->apiResponse['status'] && $this->apiResponse['response']->status)
            return TRUE;
        return FALSE;
    }

    public function getAuthorizationUrl()
    {
        return $this->apiResponse['response']->data->authorization_url;
    }
}
