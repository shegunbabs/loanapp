<?php

namespace App\Http\Requests\StandingOrder;

use App\Models\Authorization;
use App\Models\Customer;
use App\Models\SO\StandingOrder;
use App\Models\Transaction;
use Illuminate\Foundation\Http\FormRequest;

class VerifyRequest extends FormRequest
{
    public $apiResponse;
    /**
     * @var StandingOrder
     */
    private $standingOrder;
    private $standingOrderRow;
    /**
     * @var Customer
     */
    private $customer;
    /**
     * @var Authorization
     */
    private $authorization;

    /**
     * VerifyRequest constructor.
     * @param StandingOrder $standingOrder
     * @param Customer $customer
     * @param Authorization $authorization
     */
    public function __construct(StandingOrder $standingOrder, Customer $customer, Authorization $authorization)
    {
        parent::__construct();
        $this->standingOrder = $standingOrder;
        $this->customer = $customer;
        $this->authorization = $authorization;
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }


    public function save()
    {
        $ref = $this->query('reference');
        //get API response
        $this->verifyRef($ref);
        $requestPassed = FALSE;
        $transactionSuccessful = FALSE;
        $authorizationReusable = FALSE;

        if ($this->requestPassed()) {

            $data = $this->apiResponse['response']->data;

            if ($this->transactionSuccessful()) {
                //check authorization
                if ($this->authorizationReusable()) {
                    $authorizationReusable = TRUE;
                }
                $transactionSuccessful = TRUE;
            }
            $requestPassed = TRUE;
        }

        $soRow = $this->standingOrderRow;
        return compact('requestPassed', 'transactionSuccessful', 'authorizationReusable');
    }


    private function getStandingOrderRow($ref)
    {
        $this->standingOrderRow = $this->standingOrder->where('payment_reference', $ref)->first();
    }


    private function saveAuthorizationData($data)
    {
        return $this->authorization->_create($data);
    }


    private function saveTransactionData(object $data, $customer_id, $authorization_id)
    {
        return Transaction::create([
            'tid'               => $data->id,
            'domain'            => $data->domain,
            'status'            => $data->status,
            'reference'         => $data->reference,
            'amount'            => $data->amount,
            'message'           => $data->message,
            'gateway_response'  => $data->gateway_response,
            'paid_at'           => $data->paid_at,
            'channel'           => $data->channel,
            'ip_address'        => $data->ip_address,
            'fees'              => $data->fees,
            'fees_split'        => $data->fees_split,
            'authorization_id'  => $authorization_id,
            'customer_id'       => $customer_id,
            'plan'              => $data->plan,
            'transaction_date'  => $data->transaction_date,
        ]);
    }


    private function saveCustomerData($email, $data)
    {
        $d = [
            'pid' => $data->id,
            'first_name' => $data->first_name,
            'last_name' => $data->last_name,
            'email' => $data->email,
            'customer_code' => $data->customer_code,
            'phone' => $data->phone,
            'metadata' => $data->metadata,
            'risk_action' => $data->risk_action,
        ];
        $return = null;
        $c = $this->customer->where('email', $email);

        return !$c->exists() ? $this->customer->create($d) : tap($c)->update($c);

    }


    private function verifyRef($ref)
    {
        $paystack = app('PayStack');
        $res = $paystack->transaction->verify($ref);
        $this->apiResponse = $res;
    }


    private function transactionSuccessful()
    {
        if ($this->requestPassed() && $this->apiResponse['response']->data->status == "success")
            return TRUE;
        return FALSE;
    }


    private function authorizationReusable()
    {
        if ( $this->apiResponse['response']->data->authorization->reusable === TRUE )
            return TRUE;
        return FALSE;
    }


    private function requestPassed()
    {
        if ($this->apiResponse['status'] && $this->apiResponse['response']->status)
            return TRUE;
        return FALSE;
    }


    private function getCustomerDataFromApiResponse()
    {
        return $this->apiResponse['response']->data->customer;
    }

}

