<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ApRedirectIfAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //redirect authenticated users to protected pages home
        if (Auth::guard($guard)->check()) {
            return redirect('/auth/home');
        }

        //allow unauthenticated user continue
        return $next($request);
    }
}
