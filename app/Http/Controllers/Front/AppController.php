<?php

namespace App\Http\Controllers\Front;

use App\Models\App\LoanRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppController extends Controller
{

    public function home()
    {
        return view('front.user.home');
    }


    public function loanRequests()
    {
        $data = user('ap')->loanRequests()->latest()->paginate();
        return view('front.user.loan-requests', compact('data'));
    }


    public function loanRequest($loanRequestId, Request $request)
    {
        return view('front.user.loan-request');
    }


    public function showLoanRequest($requestId, Request $request)
    {
        $data = user('ap')->loanRequests()->with('applicant')->find($requestId);
        return view('front.user.loan-request', compact('data'));
    }
}
