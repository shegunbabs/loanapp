<?php

namespace App\Http\Controllers\BackOffice;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function index()
    {
        $users = User::paginate();
        return view('backoffice.user.index', compact('users'));
    }


    public function show($user)
    {
        $user = User::find($user);
        $roles = app('Bouncer')->role()->all();
        return view('backoffice.user.show', compact('user', 'roles'));
    }


    public function edit($user, Request $request)
    {
        $data = $request->except('_token');
        User::updateOrCreate(
            ['id' => $user],
            $data
        );
    }
}
