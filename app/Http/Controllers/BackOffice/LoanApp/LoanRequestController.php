<?php

namespace App\Http\Controllers\BackOffice\LoanApp;

use App\Events\LoanRequestRejected;
use App\Jobs\Remita\GetSalaryHistory;
use App\Models\App\LoanRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanRequestController extends Controller
{

    public function index(Request $request)
    {
        $statusNumber = null;
        $status = $request->query('status');
        if ($status)
            $statusNumber = $this->getLoanRequestStatus($status);

        $data = $status ? LoanRequest::where('request_status', $statusNumber)->paginate(20) : LoanRequest::paginate(20);
        return view('backoffice.loan-app.loan-requests', compact('data'));
    }


    public function show($id, Request $request)
    {
        $data = LoanRequest::with('applicant')->find($id);

        if ( count($request->query) )
            return $this->ifRequestQueryExistsRunIt($data);

        $lrs = loanRepaymentSchedule($data->interest_rate_percent, $data->tenor, $data->amount, today());
        return view('backoffice.loan-app.loan-request', compact('data', 'lrs'));
    }


    public function remitaSalaryHistory()
    {
        return view('backoffice.loan-app.remita-data');
    }


    private function getLoanRequestStatus($status)
    {
        $loanRequestStatuses = [
            'pending' => 0,
            'on-hold' => 1,
            'processing' => 2,
            'rejected' => 3,
            'processed' => 4,
            'withdrawn' => 5,
        ];
        return $loanRequestStatuses[$status];
    }

    private function ifRequestQueryExistsRunIt($loanRequestModel)
    {
        $this->rejectRequest($loanRequestModel);
        $this->checkRemitaData($loanRequestModel);
        return redirect()->back();
    }

    private function rejectRequest($loanRequestModel)
    {
        $request = app('request');
        if ($request->query('rejectRequest') && ($loanRequestModel->statusPending() || $loanRequestModel->statusOnHold() || $loanRequestModel->statusRejected()))
        {
            $loanRequestModel->setStatusRejected();
            event(new LoanRequestRejected($loanRequestModel));
            flash()->overlay('Loan Request rejected successfully');
        }
    }

    private function checkRemitaData($dataModel)
    {
        $request = app('request');
        if ($request->query('checkRemitaData') && !$dataModel->applicant->hasRemitaData()) {
            GetSalaryHistory::dispatch($dataModel->applicant->mobile, $dataModel->applicant->id);
            flash()->overlay('Remita Data request sent. Please refresh after a few seconds');
        }
    }
}
