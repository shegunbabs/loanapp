<?php

namespace App\Http\Controllers\BackOffice\Roles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Silber\Bouncer\Bouncer;

class RoleController extends Controller
{


    public function index()
    {
        $data = app('Bouncer')->role()->select('name', 'title')->get();
        return view('backoffice.roles.index', compact('data'));
    }


    public function create()
    {
        return view('backoffice.roles.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'title' => 'required|string',
        ]);

        $bouncer = Bouncer::create();
        $bouncer->role()->updateOrCreate(
            ['name' => strtolower($request->name)],
            ['title' => ucwords($request->title),]
        );

        flash()->overlay('Role created successfully');
        return redirect()->back();
    }


    public function destroy($role)
    {
        app('Bouncer')->role()->where('name', $role)->delete();
        flash()->overlay('Role deleted');
        return redirect()->back();
    }
}
