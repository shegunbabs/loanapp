<?php

namespace App\Http\Controllers\BackOffice\Roles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssignRolesToPermsController extends Controller
{


    public function index()
    {
        $roles = app('Bouncer')->role()->all();
        $abilities = app('Bouncer')->ability()->select('name', 'title')->get();

//        dd(
//            app('Bouncer')->role()->where('name', 'super_admin')->first()->getAbilities()->pluck('name')->toArray()
//        );

        return view('backoffice.roles.assign-perms', compact('roles', 'abilities'));
    }


    public function store(Request $request)
    {
        //dd($request->all());
        $role = $request->role;
        $abilities = $request->ability;

        //remove all abilities from role
        app('Bouncer')->sync($role)->abilities([]);

        if ( !is_null($abilities) ) {
            //assign abilities to role
            foreach ($abilities as $ability) {
                app('Bouncer')->allow($role)->to($ability);
            }
        }

        flash()->overlay('Abilities set.');
        return redirect()->back();
    }
}
