<?php

namespace App\Http\Controllers\BackOffice\Roles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AbilityController extends Controller
{

    public function index()
    {
        $data = app('Bouncer')->ability()->select('name', 'title')->get();
        return view('backoffice.abilities.index', compact('data'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'title' => 'required|string',
        ]);

        $bouncer = app('Bouncer');
        $bouncer->ability()->updateOrCreate(
            ['name' => strtolower($request->name)],
            ['title' => ucwords($request->title),]
        );

        flash()->overlay('Ability created successfully');
        return redirect()->back();
    }


    public function destroy($ability)
    {
        app('Bouncer')->ability()->where('name', $ability)->delete();
        flash()->overlay('Ability deleted');
        return redirect()->back();
    }
}
