<?php

namespace App\Http\Controllers\BackOffice\SO;

use App\Models\SO\StandingOrder;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SoController extends Controller
{

    public function activated()
    {
        $data = StandingOrder::where('activated', 1)->latest()->paginate();
        return view('backoffice.so.activated', compact('data'));
    }


    public function pending()
    {
        $data = StandingOrder::where('activated', 0)->latest()->paginate();
        return view('backoffice.so.pending', compact('data'));
    }


    public function invalid()
    {
        $data = StandingOrder::where('authorization_reusable', 0)->latest()->paginate();
        return view('backoffice.so.invalid', compact('data'));
    }


    public function view($payment_ref)
    {
        $data = StandingOrder::with('standingOrderRepayment')->where('payment_reference', $payment_ref)->first();
        $trans = Transaction::with('authorization')->where('reference', $data->payment_reference)->first();
        return view('backoffice.so.view', compact('data', 'trans'));
    }
}
