<?php

namespace App\Http\Controllers\StandingOrder;

use App\Http\Requests\StandingOrder\CreateRequest;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

    public function store(CreateRequest $request)
    {
        $out = $request->save();
        $res = $out['api_response'];
        if ($request->requestPassed())
            return response()->json(['authorization_url' => $request->getAuthorizationUrl()]);

        return response()->json(['success' => FALSE, 'message' => $res['response']->message]);
    }
}
