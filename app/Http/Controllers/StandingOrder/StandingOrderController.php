<?php

namespace App\Http\Controllers\StandingOrder;

use App\Http\Requests\StandingOrder\VerifyRequest;
use App\Jobs\SO\VerifyTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use ShegunBabs\PayStack\PayStack;

class StandingOrderController extends Controller
{

    public function test()
    {
        return view('bo.so.so-confirmed');
    }

    public function create()
    {
        return view('bo.so.standing-order');
    }


    public function verify(VerifyRequest $request)
    {
        $res = $request->save();

        $message = 'default message';

        if ( $res['requestPassed'] === FALSE ){
            $message = 'The request failed.';
        } else {
            VerifyTransaction::dispatch($request->query('reference'));

            if ( $res['transactionSuccessful'] === FALSE ){
                $message = "The transaction failed. Standing Order cannot be setup.";
            } else {
                if ( $res['authorizationReusable'] === TRUE ) {
                    $message = 'The Standing Order has been setup successfully.';
                } else {
                    $message = 'Authorization not reusable. The Standing Order could not be set up.';
                }
            }
        }

        flash()->overlay($message);
        return redirect()->route('so.create');
    }
}
