<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthorizationController extends Controller
{

    public function checkAuth(Request $request)
    {
        $request->validate([
            'authorization_code' => 'required',
            'amount' => 'required',
            'email' => 'required|email',
        ]);
        $paystack = app('PayStack');
        $params = [
            'authorization_code' => $request->get('authorization_code'),
            'amount' => $request->get('amount') * 100,
            'email' => $request->get('email'),
            'currency' => 'NGN'
        ];
        return $paystack->transaction->checkAuthorization($params);
    }


    public function chargeAuth(Request $request)
    {
        $request->validate([
            //'reference'             => 'required|string',
            'authorization_code'    => 'required|string',
            'amount'                => 'required',
            'email'                 => 'required|email'
        ]);
        $params = [
            'reference' => $request->get('reference'),
            'authorization_code' => $request->get('authorization_code'),
            'amount' => $request->get('amount'),
            'currency' => 'NGN',
            'email' => $request->get('email'),
        ];
        $paystack = app('PayStack');
        return $paystack->transaction->chargeAuthorization($params);
    }
}
