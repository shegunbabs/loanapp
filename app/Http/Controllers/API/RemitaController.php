<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RemitaController extends Controller
{

    public function getSalaryHistory(Request $request)
    {
        $res = app('Remita')->makeCall->getSalaryHistory($request->phoneNumber, str_random(8));
        return $res;
    }
}
