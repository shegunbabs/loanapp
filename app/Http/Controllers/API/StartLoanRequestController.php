<?php

namespace App\Http\Controllers\API;

use App\Jobs\CreateLoanRequest;
use App\Library\LoanRequestProcess;
use App\Models\Applicant;
use App\Models\BvnResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class StartLoanRequestController extends Controller
{
    /**
     * @var LoanRequestProcess
     */
    private $process;


    /**
     * StartLoanRequestController constructor.
     * @param LoanRequestProcess $process
     */
    public function __construct(LoanRequestProcess $process)
    {
        $this->process = $process;
    }

    public function index(Request $request)
    {
        //validate
        $this->validator($request->all())->validate();
        return response()->json(['response' => 'success']);
    }


    public function stepII(Request $request)
    {
        //validate
        $this->validateStepII($request->all())->validate();

        //resolve bvn
        $this->process->queryBvn($request->bvn);

        if (is_null($this->process->status)){
            return response()->json(['errors' => ['bvn' => ['Invalid BVN supplied']]], 422);
        }

        //send otp to mobile
        $res = $this->process->sendOtp();

        //getSalaryHistoryJob

        //Create Applicant Job : do after final submission

        //return response
        return response()->json([
            'otp' => $res['otp'],
            'scrambled_mobile' => $res['scrambled_mobile'],
        ]);
    }


    public function resendOtp(Request $request)
    {
        $bvn = $request->bvn;
        $res = $this->process->resendOtp($bvn);
        return response()->json([
            'otp' => $res['otp'],
            'scrambled_mobile' => $res['scrambled_mobile'],
        ]);
    }


    public function submitOtp(Request $request)
    {
        $otp = $request->otp;
        $bvn = $request->bvn;
        $res = $this->process->submitOtp($otp, $bvn);

        if ($res)
            return response()->json(['result' => TRUE]);

        return response()->json([
            'result' => (bool) $res,
            'errors' => ['otp' => ['The supplied OTP is invalid']]
        ], 422);

    }


    public function finish(Request $request)
    {
        $data = $this->process->queryBvn($request->bvn)->only('firstname', 'lastname', 'mobile', 'bvn');
        $data = array_merge($data, ['email' => $request->email, 'password' => bcrypt($request->password)]);
        //create user with details
        $applicant = Applicant::create($data);

        //create loan request [job]
        //log user in from job
        CreateLoanRequest::dispatch($applicant->id, request('loan_amount'), request('loan_tenor'));

        //log user in
        Auth::guard('ap')->login($applicant, true);

        //send response
        return response()->json(['result' => TRUE]);
    }


    private function createApplicant(){}


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'amount' => ['required'],
            'tenor' => ['required']
        ]);
    }


    protected function validateStepII(array $data)
    {
        return Validator::make($data, [
            'bvn' => ['required', 'size:11', 'unique:applicants'],
            'email' => ['required', 'email', 'unique:applicants'],
            'password' => ['required', 'min:6', 'max:50', 'confirmed']
        ]);
    }
}
