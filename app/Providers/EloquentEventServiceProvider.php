<?php

namespace App\Providers;

use App\Models\Applicant;
use App\Observers\ApplicantObserver;
use Illuminate\Support\ServiceProvider;

class EloquentEventServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Applicant::observe(ApplicantObserver::class);
    }
}
