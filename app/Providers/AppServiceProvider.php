<?php

namespace App\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use ShegunBabs\Infobip\Infobip;
use ShegunBabs\PayStack\PayStack;
use ShegunBabs\Remita\Remita;
use Silber\Bouncer\Bouncer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //Bind the PayStack Class
        $this->app->singleton('PayStack', function ($app) {
            return new PayStack(env('PAYSTACK_SECRET_KEY'));
        });

        //Bind the Bounce Class
        $this->app->singleton('Bouncer', function () {
            return Bouncer::create();
        });

        //Bind the remita class
        $merchant_id = config('services.remita.merchant_id');
        $api_key = config('services.remita.api_key');
        $api_token = config('services.remita.api_token');
        $this->app->singleton('Remita', function () use ($merchant_id, $api_key, $api_token) {
            return new Remita($merchant_id, $api_key, $api_token);
        });

        //Bind InfoBip class
        $username = env('INFOBIP_USERNAME');
        $password = env('INFOBIP_PASSWORD');
        $this->app->singleton('InfoBip', function () use ($username, $password) {
            return new Infobip($username, $password);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
