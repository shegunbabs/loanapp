<?php
/**
 * Created by PhpStorm.
 * User: shegun
 * Date: 5/15/2019
 * Time: 10:40 AM
 */

namespace App\Library;


final class Applicant
{
    /**
     * @var \App\Models\Applicant
     */
    private $applicant;


    /**
     * Applicant constructor.
     * @param \App\Models\Applicant $applicant
     */
    public function __construct(\App\Models\Applicant $applicant)
    {
        $this->applicant = $applicant;
    }


    /**
     * @param array $data, $data['firstname'], $data['lastname']
     * @return object
     */
    public function create(array $data): object
    {
        return $this->applicant->create($data);
    }


    public function getById($id)
    {
        $data = $this->applicant->find($id);
        if ($data)
            return $data;
        return null;
    }


    public function getByCustomerId($customerId)
    {
        $data = $this->applicant->where('customer_id', $customerId)->first();
        if ($data)
            return $data;
        return null;
    }


    public function disableApplicant(){}


    public function resetPassword(){}


    public function revokePassword(){}
}
