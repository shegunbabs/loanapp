<?php
/**
 * Created by PhpStorm.
 * User: shegun
 * Date: 5/20/2019
 * Time: 2:32 PM
 */

namespace App\Library;

use App\Models\Otp as OtpModel;

class Otp
{


    private $otpModel;

    private $mobile;

    private $otpToken;

    const LENGTH = 4;


    /**
     * Otp constructor.
     *
     * Initialize constructor and inject App\Models\Otp
     *
     * @param OtpModel $otpModel
     */
    public function __construct(OtpModel $otpModel)
    {
        $this->otpModel = $otpModel;
    }


    /**
     * Validate otp using otp string and mobile.
     * Also marks the table row as verified.
     *
     * @param $mobile
     * @param $otp
     * @return bool
     */
    public function validate($mobile, $otp) : bool
    {
        $data =  $this->otpModel->where('mobile', $mobile)->where('otp', $otp)->first();
        if ($data){
            $data->verified = 1;
            $data->save();
            return TRUE;
        }
        return FALSE;
    }


    /**
     * Generate & store otp for mobile
     *
     * @param $mobile
     * @param null $length
     * @return string
     */
    public function generate($mobile, $length = NULL)
    {
        $length = $length ?? self::LENGTH;
        $this->otpToken = $this->random_number($length);
        $this->mobile = $mobile;
        $this->saveOtp($mobile, $this->otpToken);
    }


    private function regenerate($length = NULL)
    {
        $length = $length ?? self::LENGTH;
    }


     /**
     * Create or update row with mobile and otp  entry.
     *
     * @param $mobile
     * @param $otp
     * @return object
     */
    private function saveOtp($mobile, $otp) : ?object
    {
        return $this->otpModel->updateOrCreate(
            ['mobile' => $mobile, 'verified' => 0],
            ['otp' => $otp,]
        );
    }


    public function sendSMS()
    {
        $sender = config('sms.sender');
        $text = sprintf("%s is your otp number", $this->getToken());
        return sendSms($sender, $this->mobile, $text);
    }


    public function sendEmail()
    {
    }


    public function getToken()
    {
        return $this->otpToken;
    }


    private function random_number($length) : string
    {
        $noOfXters = $length;
        $low = [ '4' => 1111, '5' => 11111, '6' => 111111, '7' => 1111111, '8' => 11111111, '9' => 111111111, '10' => 1111111111, ];
        $high = [ '4' => 9999, '5' => 99999, '6' => 999999, '7' => 9999999, '8' => 99999999, '9' => 999999999, '10' => 9999999999, ];
        list($usec, $sec) = explode(" ", microtime());
        $t = substr($usec, - ($noOfXters));
        $t = $t + random_int($low[$noOfXters], $high[$noOfXters]);
        return substr($t, -($noOfXters));
    }


    public function scrambleMobile()
    {
        $string = $this->mobile;
        $pattern = '/^([0-9]{4})([0-9]{3})([0-9]{4})/i';
        $replacement = '${1} XXX ${3}';
        return preg_replace($pattern, $replacement, $string);
    }
}
