<?php
/**
 * Created by PhpStorm.
 * User: shegun
 * Date: 7/4/2019
 * Time: 9:31 AM
 */

namespace App\Library;


use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputDefinition;

class ArtisanInput extends ArgvInput
{

    protected $blacklist = [
        'migrate' => [
            'rollback',
            //'refresh',
            'fresh',
            'reset',
        ],
    ];

    /**
     * ArtisanInput constructor.
     * @param array|null $argv
     * @param InputDefinition|null $definition
     */
    public function __construct(array $argv = null, InputDefinition $definition = null)
    {
        if (null === $argv) {
            $argv = $_SERVER['argv'];
        }

        /***
         * only intercept flow if there's a command after artisan
         * e.g php artisan [migrate]
         * e.g php artisan [route]
         *
         **/

        if (!empty($_SERVER['argv'][1])){
            //get the command
            $command = $_SERVER['argv'][1];

            //if the command contains argument explode it to get
            //command & argument i.e migrate:command
            $args = explode(":", $command);

            // php artisan migrate:refresh
            //if the real argument (args[1] = refresh) exists in the blacklist
            //unset the $_SERVER['argv'] variable
            //dd($args);
            if ( !empty($args[1]) && @in_array($args[1], $this->blacklist[$args[0]]) === true ) {
                unset($_SERVER['argv'][1]);
            }elseif ( @in_array($args[0], $this->blacklist) ){
                unset($_SERVER['argv'][1]);
            }
//            elseif ( array_key_exists($args[0], $this->blacklist) ) {
//                echo '3 ran' . PHP_EOL;
//                unset($_SERVER['argv'][1]);
//            }
        }

        parent::__construct($argv = null, $definition = null);
    }

    private function checkForBlackList($argument)
    {
        $command = explode(":", $argument);
        $commandOneOut = false;
        $commandTwoOut = false;



        if (array_key_exists($command[0], $this->blacklist))
            $commandOneOut = true;

        if (!empty($command[1])) {


            $commandTwoOut = true;
        }else {
            $commandTwoOut = false;
        }

        dd($commandOneOut, $commandTwoOut);

    }


}
