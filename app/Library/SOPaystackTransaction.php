<?php
/**
 * Created by PhpStorm.
 * User: shegun
 * Date: 7/3/2019
 * Time: 3:56 PM
 */

namespace App\Library;


use App\Models\Authorization;
use App\Models\Customer;
use App\Models\SO\StandingOrder;
use App\Models\Transaction;

class SOPaystackTransaction
{

    private $transaction;
    private $customer;
    private $authorization;
    private $paystack;
    private $apiResponse;
    private $standingOrder;
    private $c_data;
    private $a_data;
    private $t_data;
    private $c_savedRow;
    private $a_savedRow;
    private $t_savedRow;
    private $payment_ref;


    /**
     * PaystackTransaction constructor.
     * @param Transaction $transaction
     * @param Customer $customer
     * @param Authorization $authorization
     */
    public function __construct(Transaction $transaction, Customer $customer, Authorization $authorization, StandingOrder $standingOrder)
    {
        $this->transaction = $transaction;
        $this->customer = $customer;
        $this->authorization = $authorization;
        $this->standingOrder = $standingOrder;

        $this->paystack = app('PayStack');
    }


    public function verifyTrans($payment_ref)
    {
        if (is_null($payment_ref)) {
            return $this->throwException("Please provide payment reference");
        }

        $this->payment_ref = $payment_ref;
        //verify transaction
        $this->apiResponse = $this->paystack->transaction->verify($payment_ref);
        if ($this->apiResponsePassed()):
            $this->t_data = $this->apiResponse['response']->data;
            $this->a_data = $this->t_data->authorization;
            $this->c_data = $this->t_data->customer;
        else:
            $this->saveInvalidRequest();
        endif;

        $this->saveCustomerData();
        $this->saveAuthorization();
        $this->saveTransaction();
    }


    public function updateSORow()
    {
        //get SO Row
        $SORow = $this->standingOrder->where('payment_reference', $this->payment_ref)->first();

        //confirmPayment() successful payment on SO Row
        //finishGlobal() if transaction failed
        $this->transactionSuccessful()
            ? $SORow->confirmPayment()
            : $SORow->finishGlobal();

        //update setAuthReusable() for SO Row
        //doStandingOrderRepaymentSchedule() if auth is reusable
        if ($this->authorizationReusable()) :
            $SORow->setAuthReusable(true);
            $SORow->doStandingOrderRepaymentSchedule();
        else :
            $SORow->setAuthReusable(false);
        endif;

        //update SO Row with transaction_id
        $SORow->transaction_id = $this->t_savedRow->id;
        $SORow->save();


        //finishProcess() for all
        $SORow->finishProcess();
    }


    public function updateRepaymentDetail()
    {

    }


    public function __toString()
    {

    }


    public function checkAuthorization()
    {

    }


    public function chargeAuthorization($apiResponse)
    {

    }


    private function throwException($msg = "Exception Message...")
    {
        throw new \Exception($msg);
    }

    private function apiResponsePassed()
    {
        if ($this->apiResponse['status'] && $this->apiResponse['response']->status)
            return true;
        return false;
    }

    private function transactionSuccessful()
    {
        if ($this->apiResponsePassed() && $this->apiResponse['response']->data->status == "success")
            return true;
        return false;
    }

    private function authorizationReusable()
    {
        return $this->a_data->reusable === true ? (bool)true : (bool)false;
    }

    private function customerDataExists()
    {
        return $this->c_data ? (bool)true : (bool)false;
    }

    private function saveCustomerData()
    {
        if ($this->customerDataExists())
            return $this->c_savedRow = $this->customer->_create($this->c_data);
    }

    private function authorizationDataExists()
    {
        return $this->a_data ? (bool)true : (bool)false;
    }

    private function saveAuthorization()
    {
        if ($this->authorizationDataExists())
            $this->a_savedRow = $this->authorization->_create($this->a_data);
    }

    private function transactionDataExists()
    {
        return $this->t_data ? (bool)true : (bool)false;
    }

    private function saveTransaction()
    {
        if ($this->transactionDataExists())
            $this->t_savedRow = $this->transaction->_create($this->t_data, $this->c_savedRow->id, $this->a_savedRow->id);
    }

    private function saveInvalidRequest()
    {
        $data = (object) array_merge( (array) $this->apiResponse['response'], ['reference' => $this->payment_ref] );
        $this->transaction->_create($data);
    }

}
