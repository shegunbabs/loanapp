<?php
/**
 * Created by PhpStorm.
 * User: shegun
 * Date: 6/7/2019
 * Time: 11:43 AM
 */

namespace App\Library\Contracts\Response;


interface Api
{

    public function requestPassed();


    public function transactionSuccessful();


    public function AuthorizationReusable();
}
