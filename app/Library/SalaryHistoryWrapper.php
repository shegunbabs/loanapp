<?php
/**
 * Created by PhpStorm.
 * User: shegun
 * Date: 6/25/2019
 * Time: 1:46 PM
 */

namespace App\Library;


use App\Models\Remita\RemitaData;
use App\Models\Remita\RemitaLoanHistory;
use App\Models\Remita\RemitaSalaryPayment;
use App\Models\Remita\SalaryHistoryResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SalaryHistoryWrapper
{
    /**
     * @var SalaryHistoryResponse
     */
    private $salaryHistoryResponse;
    /**
     * @var RemitaData
     */
    private $remitaData;
    /**
     * @var RemitaSalaryPayment
     */
    private $salaryPayment;
    /**
     * @var RemitaLoanHistory
     */
    private $loanHistory;
    private $data;


    /**
     * SalaryHistoryWrapper constructor.
     * @param SalaryHistoryResponse $salaryHistoryResponse
     * @param RemitaData $remitaData
     * @param RemitaSalaryPayment $salaryPayment
     * @param RemitaLoanHistory $loanHistory
     */
    public function __construct(SalaryHistoryResponse $salaryHistoryResponse, RemitaData $remitaData, RemitaSalaryPayment $salaryPayment, RemitaLoanHistory $loanHistory)
    {
        $this->salaryHistoryResponse = $salaryHistoryResponse;
        $this->remitaData = $remitaData;
        $this->salaryPayment = $salaryPayment;
        $this->loanHistory = $loanHistory;
    }


    public function query($phone, $applicant_id = null)
    {
        $remitaDataRow = $this->salaryHistoryResponse->where('mobile', $phone)->first();

        if ($remitaDataRow) {
            //$this->castRemitaData($remitaDataRow);
            $this->data = $remitaDataRow->toArray();
            return $this;
        }

        $response = app('Remita')->makeCall->getSalaryHistory($phone, str_random(8));
        $this->transformData($response['response'], $phone, $applicant_id);
        $this->saveToDb();
        return $this;
    }


    private function transformData($response, $phone, $applicant = null)
    {
        $data = objToArray($response, $data);

        $newVars = $data;

        if (is_null($newVars["data"])) {
            $new = is_null($applicant) ? ['mobile' => $phone] : ['mobile' => $phone, 'applicant_id' => $applicant];
            $this->data = array_merge($this->snakeArray($newVars), $new);
            return;
        }

        unset($newVars['data']);
        $newVars = array_merge($newVars, $data["data"]);
        $return = $this->snakeArray($newVars);
        $this->data = array_merge($return, ['mobile' => $phone]);
        if ($applicant)
            $this->data = array_merge($this->data, ['applicant_id' => $applicant]);
    }


    private function saveToDb()
    {
        DB::transaction(function(){

            $this->salaryHistoryResponse->updateOrCreate(
                ['mobile' => $this->get('mobile')],
                $this->except('request_date', 'bvn', 'applicant_id', 'data')
            );

            if ($this->get('applicant_id') && $this->get('has_data') && $this->get('status')) {
                //insert remita data
                $remitaDataModel = $this->remitaData->create(
                    $this->only('applicant_id', 'account_number', 'customer_id', 'mobile', 'bank_code', 'customer_name', 'company_name', 'category', 'first_payment_date')
                );

                $salary_payments = $this->get('salary_payment_details');
                if ($salary_payments):
                    foreach ($salary_payments as $item) {
                        //insert salaryPayments
                        $remitaDataModel->remitaSalaryPayments()->create([
                            'payment_date' => $item['paymentDate'],
                            'amount' => $item['amount'],
                            'account_number' => $item['accountNumber'],
                            'bank_code' => $item['bankCode']
                        ]);
                    }
                endif;

                $existing_loans = $this->get('loan_history_details');
                if ($existing_loans):
                    foreach ($existing_loans as $item) {
                        //insert existingLoan
                        $remitaDataModel->remitaLoanHistory()->create([
                            'status' => $item['status'],
                            'provider' => $item['loanProvider'],
                            'amount' => $item['loanAmount'],
                            'outstanding_amount' => $item['outstandingAmount'],
                            'date_disbursed' => $item['loanDisbursementDate'],
                            'repayment_amount' => $item['repaymentAmount'],
                            'repayment_frequency' => $item['repaymentFreq'],
                        ]);
                    }
                endif;
            }
        });
    }


    /**
     * @param $remitaDataRow
     */
    private function castRemitaData($remitaDataRow)
    {


        if ($remitaDataRow instanceof RemitaData) {

        }
    }


    public function except($value)
    {
        $values = $this->all();

        $args = func_get_args();
        foreach ($args as $arg) {
            unset($values[$arg]);
        }

        return $values;
    }

    public function all()
    {
        return $this->data;
    }

    public function only()
    {
        $out = [];
        $args = func_get_args();
        foreach ($args as $arg) {
            ($this->data[$arg] || $this->data[Str::snake($arg)]) ? $out[$arg] = $this->data[$arg] : $out[$arg] = null;
        }
        return $out;
    }

    public function get($value)
    {
        return $this->__get($value);
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data))
            return $this->data[$name];
        return null;
    }

    private function snakeArray(array $data)
    {
        $return = [];
        foreach ($data as $key => $value) {
            $return{Str::snake($key)} = $value;
        }
        return $return;
    }
}
