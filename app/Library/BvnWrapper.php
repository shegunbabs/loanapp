<?php
/**
 * Created by PhpStorm.
 * User: shegun
 * Date: 5/21/2019
 * Time: 1:46 PM
 */

namespace App\Library;

use App\Models\BvnResponse;

class BvnWrapper
{
    /**
     * @var Bvn
     */
    private $bvn;
    /**
     * @var BvnResponse
     */
    private $bvnResponseModel;
    private $response;
    private $out;


    /**
     * BvnWrapper constructor.
     * @param Bvn $bvn
     * @param BvnResponse $bvnResponseModel
     */
    public function __construct(BvnResponse $bvnResponseModel)
    {
        $this->bvnResponseModel = $bvnResponseModel;
    }


    public function queryBvn($bvnNumber, $force = FALSE)
    {
        //find bvn first
        $bvnRow = $this->bvnResponseModel->findBvn($bvnNumber);

        //if row exists return it
        if ($bvnRow) {
            //format data for output
            $this->formatOutputData($bvnRow);
            return $this;
        }


        //if not exists; query API
        $response = app('PayStack')->verification->resolveBVN($bvnNumber);

        //if API response valid
        if ($response['status'] && $response['http_status'] === 200) {
            //and save to database
            $this->saveToDb($response['response']);

            //format data for output
            $this->formatOutputData($response['response']);
        } else {
            $this->out['status'] = null;
        }
        return $this;
    }


    private function saveToDb($fields)
    {
        $res = $fields;
        $this->out = [
            'status' => $res->status,
            'message' => $res->message,
            'first_name' => $res->data->first_name,
            'last_name' => $res->data->last_name,
            'dob' => $res->data->dob,
            'formatted_dob' => $res->data->formatted_dob,
            'mobile' => $res->data->mobile,
            'bvn' => $res->data->bvn,
        ];
        $copy = $this->out;
        unset($copy['bvn']);
        $this->bvnResponseModel->updateOrCreate(
            ['bvn' => $res->data->bvn],
            $copy
        );
    }


    //@TODO Implement forceQuery
    private function forceQuery()
    {

    }


    protected function formatOutputData($info)
    {
        if ($info instanceof BvnResponse) {
            $data = $info;
            $this->out['message'] = $data->message;
            $this->out['status'] = (bool) $data->status;
        } else {
            $data = $info->data;
            $this->out['message'] = $info->message;
            $this->out['status'] = (bool) $info->status;
        }
        $this->out['first_name'] = $data->first_name;
        $this->out['last_name'] = $data->last_name;
        $this->out['dob'] = $data->dob;
        $this->out['mobile'] = $data->mobile;
        $this->out['bvn'] = $data->bvn;
        $this->out['formatted_dob'] = $data->formatted_dob;
    }


    public function getResponse()
    {
        return $this->out;
    }
}
