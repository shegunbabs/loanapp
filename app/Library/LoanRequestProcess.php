<?php
/**
 * Created by PhpStorm.
 * User: shegun
 * Date: 5/15/2019
 * Time: 5:22 PM
 */

namespace App\Library;

use App\Models\BvnResponse;
use Illuminate\Support\Arr;

class LoanRequestProcess
{
    /**
     * @var Bvn
     */
    private $bvn;

    protected $bvnNumber;
    protected $message, $status, $firstname, $lastname, $dob, $formatted_dob, $mobile;
    /**
     * @var Otp
     */
    private $otp;
    /**
     * @var BvnWrapper
     */
    private $bvnWrapper;


    /**
     * Process constructor.
     * @param BvnWrapper $bvnWrapper
     * @param Otp $otp
     */
    public function __construct(BvnWrapper $bvnWrapper, Otp $otp)
    {
        $this->otp = $otp;
        $this->bvnWrapper = $bvnWrapper;
    }


    public function queryBvn($bvnNumber)
    {
        //query the API or Database and return response
        $bvnData = $this->bvnWrapper->queryBvn($bvnNumber)->getResponse();

        //if status === true (if there is valid data) castData (populate the class with data)
        if ($bvnData['status'])
            $this->castData($bvnData);
        return $this;
    }


    /**
     * Send OTP to BVN's Mobile
     * @return array
     */
    public function sendOtp()
    {
        $this->otp->generate($this->mobile);
        //$this->otp->sendSMS();
        return [
            'scrambled_mobile' => $this->otp->scrambleMobile(),
            'otp' => $this->otp->getToken(),
            ];
    }


    public function resendOtp($bvn)
    {
        $bvnResponse = BvnResponse::where('bvn', $bvn)->first();
        $this->otp->generate($bvnResponse->mobile);
        //$this->otp->sendSMS();
        return [
            'scrambled_mobile' => $this->otp->scrambleMobile(),
            'otp' => $this->otp->getToken(),
        ];
    }

    public function submitOtp($otp, $bvn)
    {
        $mobile = BvnResponse::where('bvn', $bvn)->first()->mobile;
        return $this->otp->validate($mobile, $otp);
    }

    private function castData($data)
    {
        $this->bvnNumber = $data['bvn'];
        $this->status = $data['status'];
        $this->firstname = $data['first_name'];
        $this->lastname = $data['last_name'];
        $this->dob = $data['dob'];
        $this->formatted_dob = $data['formatted_dob'];
        $this->mobile = $data['mobile'];
        $this->message = $data['message'];
    }

    public function all()
    {
        return [
            'bvn' => $this->bvnNumber,
            'status' => $this->status,
            'message' => $this->message,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'dob' => $this->dob,
            'mobile' => $this->mobile,
            'formatted_dob' => $this->formatted_dob,
        ];
    }

    public function get($key)
    {
        return $this->__get($key);
    }

    public function only()
    {
        $out=[];
        $args = func_get_args();
        foreach ($args as $arg) {
            $out[$arg] = $this->all()[$arg];
        }
        return $out;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->all()))
            return $this->all()[$name];
    }
}
