<?php
/**
 * Created by PhpStorm.
 * User: shegun
 * Date: 6/27/2019
 * Time: 3:14 PM
 */

namespace App\Library;


use App\Models\Applicant;
use App\Models\BvnResponse;
use App\Models\ScoringData;
use Carbon\Carbon;

class DoScoring
{
    /**
     * @var Applicant
     */
    private $applicant;
    /**
     * @var ScoringData
     */
    private $scoringData;
    private $data;

    /**
     * DoScoring constructor.
     * @param Applicant $applicant
     */
    public function __construct(Applicant $applicant, ScoringData $scoringData)
    {
        $this->applicant = $applicant;
        $this->scoringData = $scoringData;
    }


    public function score($applicant_id)
    {
        $this->data = $this->applicant->find($applicant_id);

        $age = $this->calculateAge();
        $allowable_age = config('loanapp.allowable_age');

        $salary = $this->applicant->remitaData->remitaSalaryPayments()->latest()->first()->amount;
        $averageSalary = $this->applicant->remitaData->remitaSalaryPayments->avg('amount');

        $salaryPercent = config('loanapp.allowable_salary_percent_amount');
        $salaryPercentAmount = $salaryPercent * $averageSalary;


        $this->applicant->scoringData->updateOrCreate(
            ['applicant_id' => $this->applicant->id],
            [
                'allowable_age' => $allowable_age,
                'age' => $age,
                'salary_amount' => $salary,
                'average_salary_amount' => $averageSalary,
                'allowable_salary_percent' => $salaryPercent,
                'allowable_salary_percent_amount' => $salaryPercentAmount,
            ]);
        return $this;
    }

    private function calculateAge()
    {
        $data = BvnResponse::where('mobile', $this->data->mobile)->where('bvn', $this->data->bvn)->first();
        $dob = Carbon::createFromFormat("Y-m-d H:i:s", $data->formatted_dob);
        return $dob->diffInYears(Carbon::today());
    }
}
