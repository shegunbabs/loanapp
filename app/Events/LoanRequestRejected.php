<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LoanRequestRejected
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $loanRequestModel;

    /**
     * Create a new event instance.
     *
     * @param $loanRequestModel
     */
    public function __construct($loanRequestModel)
    {
        //
        $this->loanRequestModel = $loanRequestModel;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
