<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'back-office:create_user {name} {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Loanapp Backoffice User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param User $user
     * @return mixed
     */
    public function handle(User $user)
    {
        $data = $user->updateOrCreate(
            ['email' => $this->argument('email')],
            [
                'name' => ucwords($this->argument('name')),
                'email' => strtolower($this->argument('email')),
                'password' => bcrypt($this->argument('password')),
            ]);
        if ($data){
            $this->info('User created successfully') . PHP_EOL;
        }

    }
}
