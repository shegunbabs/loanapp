<?php

namespace App\Console\Commands;

use App\Models\SO\StandingOrder;
use App\Models\SO\StandingOrderRepayment;
use Illuminate\Console\Command;

class ProcessRepayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'standing_order:process_payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process payments awaiting debit on all standing orders';
    /**
     * @var StandingOrderRepayment
     */
    private $standingOrderRepayment;

    /**
     * Create a new command instance.
     *
     * @param StandingOrderRepayment $standingOrderRepayment
     */
    public function __construct(StandingOrderRepayment $standingOrderRepayment)
    {
        parent::__construct();
        $this->standingOrderRepayment = $standingOrderRepayment;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get all standing order repayments due for today
        $due = $this->standingOrderRepayment->where('transaction_date', today())->get();

        //dd($due);



        foreach ($due as $item)
        {
            //get params ready authorization_code
            $item->checkAuthorization();

            if ($item->authorization_check_status)
                //charge authorization
                $item->processTransaction();



        }
    }
}
