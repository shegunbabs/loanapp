<?php

namespace App\Notifications\App;

use App\Models\App\LoanRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class LoanRequestRejected extends Notification implements ShouldQueue
{
    use Queueable;
    /**
     * @var LoanRequest
     */
    public $loanRequest;

    /**
     * Create a new notification instance.
     *
     * @param LoanRequest $loanRequest
     */
    public function __construct(LoanRequest $loanRequest)
    {
        //
        $this->loanRequest = $loanRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Hello!,')
            ->subject('Loan Request Status')
            ->line('Your loan request has been rejected. ')
            //->action('Check Dashboard', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => 'Your loan request was not successful.'
        ];
    }
}
