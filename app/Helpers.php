<?php
/**
 * Created by PhpStorm.
 * User: shegun
 * Date: 5/17/2019
 * Time: 11:08 AM
 **/


function roundNearestHundredUp($number)
{
    return ceil( $number / 100 ) * 100;
}

function pv($rate, $nper, $pmt, $fv=0, $type=0)
{
    $pvif = $pvif = pow(1 + $rate, $nper);
    $pv = (($pvif - 1)/$rate) * ($pmt / ($pvif + $fv));
    return $pv;
}


function objToArray($obj, &$arr){

    if(!is_object($obj) && !is_array($obj)){
        $arr = $obj;
        return $arr;
    }

    foreach ($obj as $key => $value)
    {
        if (!empty($value))
        {
            $arr[$key] = array();
            objToArray($value, $arr[$key]);
        }
        else
        {
            $arr[$key] = $value;
        }
    }
    return $arr;
}


function loanRepaymentSchedule($interestRate, $tenor, $loanAmount, $startDate)
{
    $out['details'] = [
        'interest_rate' => (float) $interestRate,
        'tenor' => (int) $tenor,
        'loan_amount' => (int) $loanAmount,
        'date' => $startDate,
    ];
    $interestRate = $interestRate/100;
    $monthly_repayment = emi($interestRate, $tenor, $loanAmount);

    $date_format = "d-M-Y";
    $due_date = \Carbon\Carbon::parse($startDate) ->format($date_format);
    for ($i = 1; $i <= $tenor; $i++) {
        $one_month = emi($interestRate, 1, $loanAmount);
        $interest = $one_month - $loanAmount;
        $principal = $monthly_repayment - $interest;
        $total = $principal + $interest;

        $out['data'][] = [
            'due_date' => $due_date,
            'principal' => $principal,
            'interest' => $interest,
            'total' => $total,
        ];


        $loanAmount = $loanAmount - $principal;
        $due_date = \Carbon\Carbon::parse($due_date)->addMonth()->format($date_format);
    }
    return $out;

}


if (!function_exists('emi')) {

    // e.g
    function emi($rate, $nper, $pv, $fv = 0, $type = 0)
    {
        if (!$fv) $fv = 0;
        if (!$type) $type = 0;
        if ($rate == 0) return -($pv + $fv) / $nper;
        $pvif = pow(1 + $rate, $nper);
        $pmt = $rate / ($pvif - 1) * ($pv * $pvif + $fv); //- ($pv * $pvif + $fv)
        if ($type == 1) {
            $pmt /= (1 + $rate);
        }
        return $pmt;
    }
}


if (!function_exists('user')) {
    function user($guard = null)
    {
        return $guard === null ? auth()->user() : auth($guard)->user();
    }
}


function sendSms($sender, $recipient, $message)
{

    if (!(bool)config('sms.send_sms'))
        return "OK";

    $username = 'shegun.babs';
    $password = 'seges123';

    $data = [
        'username' => $username,
        'password' => $password,
        'message' => $message,
        'sender' => $sender,
        'mobiles' => $recipient,
    ];

    $opts = [
        'http' => [
            'method' => 'GET',
        ]
    ];

    $context = stream_context_create($opts);
    $url = "https://account.kudisms.net/api/?" . http_build_query($data);
    $return = file_get_contents($url, false, $context);

    $return = json_decode($return);

    $response = $return->status ?? null;
    $err = $return->error ?? null;

    return $response ?? $err;
}


function localPhoneNumber($number)
{
    //if 1st xter starts from 7|8|9
    //append 0 and return

    //if 1st 2 xters are 07|08|09
    //return same
}


function intlPhoneNumber($number)
{
    //if 1st 2 xters are 07|08|09
    //remove leading 0 and append 234
    return;
}


function toNaira($amount, $transform = TRUE, $decimal = 2, $symbol = '₦')
{
    $out = '';
    if ($transform === TRUE) {
        $out = number_format($amount, $decimal);
    } else {
        $out = $amount;
    }

    return $symbol . $out;
}


function fakeMobile()
{
    $net = random_int(8,9);
    $net2 = random_int(20000000, 90000000);
    return '0'.$net.'0'.$net2;
    //[0][8][0][20000000-90000000]
}


function fakeLoanAmount()
{
    $thousand = [
        '000',
        '0000',
        '00000'
    ];
    $fig = random_int(1, 9);
    $thou = random_int(1, count($thousand)-1);
    return $fig.$thousand[$thou];
}
