<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname', 40);
            $table->string('lastname', 40);
            $table->string('email')->unique();
            $table->string('mobile', 16);
            $table->string('bvn', 11);
            $table->string('api_token', 60);
            $table->string('password');
            $table->tinyInteger('default_password')->default(0);
            $table->tinyInteger('disabled')->default(0);
            $table->tinyInteger('change_password')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
