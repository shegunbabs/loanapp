<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tid')->nullable();
            $table->string('domain')->nullable();
            $table->string('status');
            $table->string('reference');
            $table->string('amount');
            $table->string('message')->nullable();
            $table->string('gateway_response');
            $table->timestamp('paid_at')->nullable();
            $table->string('channel');
            $table->string('ip_address')->nullable();
            $table->string('fees')->nullable();
            $table->string('fees_split')->nullable();
            $table->string('authorization_id')->nullable();
            $table->string('customer_id')->nullable();
            $table->string('plan')->nullable();
            $table->string('transaction_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
