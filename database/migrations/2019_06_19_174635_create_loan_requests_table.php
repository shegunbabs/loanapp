<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('applicant_id');
            $table->string('amount');
            $table->string('tenor');
            $table->string('interest_rate_percent')->default(7);
            $table->string('monthly_amount_payable');
            $table->string('total_amount_payable');
            $table->tinyInteger('request_status')->default(0);
            $table->tinyInteger('offer_status')->default(0);
            $table->timestamp('offer_sent_date')->nullable();
            $table->timestamp('offer_processed_date')->nullable();
            $table->timestamps();

            $table->foreign('applicant_id')
                ->references('id')
                ->on('applicants')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_requests');
    }
}
