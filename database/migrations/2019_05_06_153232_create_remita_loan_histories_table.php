<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitaLoanHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remita_loan_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('remita_data_id')->nullable();
            $table->string('status', 20);
            $table->string('provider', 100);
            $table->string('amount');
            $table->string('outstanding_amount');
            $table->timestamp('date_disbursed')->nullable();
            $table->string('repayment_amount');
            $table->string('repayment_frequency');  //MONTHLY
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remita_loan_histories');
    }
}
