<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandingOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standing_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id')->default(0);
            $table->string('beneficiary');
            $table->string('purpose');
            $table->string('payment_type');
            $table->string('payment_frequency');
            $table->string('amount');
            $table->timestamp('start_date')->nullable();
            $table->string('no_of_debit');
            $table->timestamp('end_date')->nullable();
            $table->string('payers_name');
            $table->string('payers_phone');
            $table->string('payers_email');
            $table->string('funding_source');
            $table->string('total_amount');
            $table->tinyInteger('activated')->default(0);
            $table->tinyInteger('disabled')->default(0);
            $table->string('payment_reference')->nullable();
            $table->tinyInteger('payment_confirmed')->default(0);
            $table->tinyInteger('process_finished')->default(0);
            $table->tinyInteger('global_finished')->default(0);
            $table->tinyInteger('authorization_reusable')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standing_orders');
    }
}
