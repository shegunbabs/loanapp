<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepaymentSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repayment_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('loan_disbursement_id');
            $table->unsignedBigInteger('customer_id');
            $table->timestamp('payment_date')->nullable();
            $table->string('amount');
            $table->integer('payment_status');
            $table->string('mandate_ref');
            $table->string('payment_ack');
            $table->string('amount_expected');
            $table->timestamps();

            $table->foreign('loan_disbursement_id')->references('id')->on('loan_disbursements')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repayment_schedules');
    }
}
