<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('applicant_id');
            $table->unsignedBigInteger('loan_request_id');
            $table->string('amount');
            $table->string('tenor');
            $table->integer('interest_rate_percent')->default(7);
            $table->string('monthly_amount_payable');
            $table->string('total_amount_payable');
            $table->timestamp('collection_date')->nullable();
            $table->string('account_number');
            $table->string('bank_name');
            $table->string('average_salary');
            $table->string('net_salary');
            $table->string('outstanding_loan_amount');
            $table->string('outstanding_loan_monthly_payment');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();

            $table->foreign('applicant_id')->references('id')->on('applicants')->onDelete('cascade');
            $table->foreign('loan_request_id')->references('id')->on('loan_requests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_applications');
    }
}
