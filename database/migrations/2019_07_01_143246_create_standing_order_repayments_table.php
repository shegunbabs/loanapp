<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandingOrderRepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standing_order_repayments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id')->default(0);
            $table->unsignedBigInteger('standing_order_id');
            $table->timestamp('transaction_date')->nullable();
            $table->string('transaction_amount');
            $table->string('amount');
            $table->string('payment_reference')->nullable();
            $table->string('authorization_check_status')->nullable();
            $table->string('authorization_check_message')->nullable();
            $table->timestamp('authorization_check_last_timestamp')->nullable();
            $table->tinyInteger('payment_confirmed')->default(0);
            $table->tinyInteger('processed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standing_order_repayments');
    }
}
