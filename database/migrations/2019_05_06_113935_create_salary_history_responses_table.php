<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryHistoryResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_history_responses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mobile', 16);
            $table->string('status', 10)->nullable();
            $table->string('has_data', 6);
            $table->string('response_id', 50);
            $table->timestamp('response_date')->nullable();
            $table->string('response_code', 10);
            $table->string('response_msg', 50);
            $table->string('customer_id', 15)->nullable();
            $table->string('account_number', 11)->nullable();
            $table->string('bank_code', 10)->nullable();
            $table->string('company_name')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('category')->nullable();
            $table->timestamp('first_payment_date')->nullable();
            $table->integer('salary_count')->nullable();
            $table->text('salary_payment_details')->nullable();
            $table->text('loan_history_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_history_responses');
    }
}
