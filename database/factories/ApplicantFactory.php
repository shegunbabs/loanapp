<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Applicant::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'mobile' => fakeMobile(),
        'bvn' => random_int(2222222222, 9999999999),
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
