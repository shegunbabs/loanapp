<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Models\App\LoanRequest::class, function (Faker $faker) {

    return [
        'applicant_id' => factory(App\Models\Applicant::class)->create()->id,
        'amount' => $am = fakeLoanAmount(),
        'tenor' => $tenor = $faker->numberBetween(1, 6),
        'interest_rate_percent' => $rate = $faker->numberBetween(5, 7),
        'monthly_amount_payable' => $map = emi($rate/100, $tenor, $am),
        'total_amount_payable' => $map * $tenor,
    ];
});
