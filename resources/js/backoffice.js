window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

//import ElementUI from 'element-ui'
//import VueNumeric from 'vue-numeric'

window.Vue = require('vue');


// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('loan-request-status', require('./components/LoanRequestStatus').default);
Vue.component('loan-request-offer-status', require('./components/LoanRequestOfferStatus').default);
Vue.component('remita-salary-history', require('./components/RemitaSalaryHistory').default);

// Vue.filter('money_format', function(value){
//     if (!value) return '';
//     let $value = parseFloat(value).toFixed(2).toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
//     return $value;
// });

// import locale from 'element-ui/lib/locale/lang/en'

// Vue.use(ElementUI, { locale });
// Vue.use(VueNumeric)

const app = new Vue({
    el: '#app',
});

$('#flash-overlay-modal').modal();
$('div.alert').not('.alert-important').delay(7000).fadeOut(550);
