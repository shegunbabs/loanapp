/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import ElementUI from 'element-ui'
import VueNumeric from 'vue-numeric'

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('remita-salary-history', require('./components/RemitaSalaryHistory').default);
Vue.component('instant-loan', require('./components/InstantLoan').default);
Vue.component('standing-order', require('./components/StandingOrder').default);
Vue.component('loan-request-status', require('./components/LoanRequestStatus').default);
Vue.component('loan-request-offer-status', require('./components/LoanRequestOfferStatus').default);
Vue.component('loan-request', require('./components/LoanRequest').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.filter('money_format', function(value){
    if (!value) return '';
    let $value = parseFloat(value).toFixed(2).toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return $value;
});

import locale from 'element-ui/lib/locale/lang/en'

Vue.use(ElementUI, { locale });
Vue.use(VueNumeric)

const app = new Vue({
    el: '#app',
});

$('#flash-overlay-modal').modal();
$('div.alert').not('.alert-important').delay(7000).fadeOut(550);
