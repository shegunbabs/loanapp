@extends('layouts.so.master')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12 col-12">
            <div class="card standing-order">
                <div class="card-header font-medium text-lg">
                    Standing Order in favor of QuickFund MFB.
                </div>

                <div class="card-body">

                    <div class="row justify-content-center">
                        <div class="col-12 col-md-10">
                            <div class="alert alert-important alert-icon alert-success" role="alert">
                                <i class="fe fe-check mr-2" aria-hidden="true"></i>
                                Initial payment for the standing order has been successfully verified. However the standing order still needs to be activated.
                            </div>

                            <div class="alert alert-important alert-icon alert-danger" role="alert">
                                <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i>
                                Initial payment for the standing order has been successfully verified. However the Authorization received cannot be used to setup a standing order. Please contact the
                                Admin.
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-12 col-md-10">
                            <h4>Standing Order Information</h4>
                            <table class="table">
                                <tr>
                                    <td class="font-bold">Payer's Name</td>
                                    <td>Alphonsus Adeniyi</td>
                                </tr>
                                <tr>
                                    <td class="font-bold">Funding Source</td>
                                    <td>Card</td>
                                </tr>
                                <tr>
                                    <td class="font-bold">Card Type</td>
                                    <td>Visa - **** **** **** 4685</td>
                                </tr>
                                <tr>
                                    <td class="font-bold">Authorization Reusable?</td>
                                    <td class="font-bold">No</td>
                                </tr>
                                <tr>
                                    <td class="font-bold">Amount</td>
                                    <td>{{ toNaira(21500) }}</td>
                                </tr>
                                <tr>
                                    <td class="font-bold">Start Date</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="font-bold">End Date</td>
                                    <td></td>
                                </tr>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
