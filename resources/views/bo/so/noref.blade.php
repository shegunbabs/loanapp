@extends('layouts.so.master')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 col-12">
            <div class="alert alert-icon alert-danger" role="alert">
                <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i>
                Unknown error has occurred.
            </div>
            <a href="{{route('so.create')}}" class="btn btn-primary">Go to Standing Order Home</a>
        </div>
    </div>

@endsection
