@extends('layouts.backoffice.auth')

@section('content')

    <div class="card ">
        <div class="card-header text-center">
            <a href="#">
                <h1>BackOffice</h1>
                <span class="splash-description">Please enter your user information.</span>
            </a>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-group">
                    <input class="form-control form-control-lg @error('email') is-invalid @enderror"
                           id="email" type="email" placeholder="Email Address" autocomplete="off" name="email"
                           value="{{ old('email') }}" required autofocus
                    >
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg @error('password') is-invalid @enderror"
                           id="password" type="password" name="password"
                           placeholder="Password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="custom-control-label">Remember Me</span>
                    </label>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
            </form>
        </div>
        <div class="card-footer bg-white p-0  ">
            <div class="card-footer-item card-footer-item-bordered">
                <a href="#" class="footer-link">Create An Account</a></div>
            <div class="card-footer-item card-footer-item-bordered">
                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}" class="footer-link">Forgot Password</a>
                @endif
            </div>
        </div>
    </div>

@endsection
