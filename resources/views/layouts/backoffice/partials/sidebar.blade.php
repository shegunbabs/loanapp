<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        Menu
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" ><i class="fa fa-fw fa-user-circle"></i>Dashboard
                            <span class="badge badge-success">6</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2">
                            <i class="fa fa-fw fa-rocket"></i>Standing Orders
                        </a>
                        <div id="submenu-2" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('so.activated') }}">Activated <span class="badge badge-secondary">New</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('so.pending') }}">Pending</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('so.invalid') }}">Invalid</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#roles-perms" aria-controls="roles-perms">
                            <i class="fa fa-fw fa-rocket"></i>Roles & Perms
                        </a>
                        <div id="roles-perms" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('roles.index') }}">Roles <span class="badge badge-secondary">New</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('abilities.index') }}">Permissions</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('roles.assign') }}">Assign</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#loans" aria-controls="loans">
                            <i class="fa fa-fw fa-rocket"></i>Loans
                        </a>
                        <div id="loans" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('bo.loan.requests') }}">Loan Requests <span class="badge badge-secondary">New</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Loan Applications</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Disbursements</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Repayments</a>
                                </li>

                            </ul>
                        </div>
                    </li>

                </ul>
            </div>
        </nav>
    </div>
</div>
