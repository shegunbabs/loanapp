<div class="header py-2">
    <div class="container">
        <div class="d-flex">
            <a class="header-brand" href="#">
                <img src="/img/quickpay.svg" class="header-brand-img" alt="QuickPay logo">
            </a>
            <div class="d-flex order-lg-2 ml-auto">
                @if( auth('ap')->user() )

                    @if( ! user('ap')->loanRequests()->latest()->first()->hasLoanRequest() )
                        <loan-request>Loading...</loan-request>
                    @endif

                    <div class="dropdown d-none d-md-flex">
                        <a class="nav-link icon" data-toggle="dropdown" aria-expanded="false">
                            <i class="fe fe-bell"></i>
                            <span class="nav-unread"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" x-placement="bottom-end"
                             style="position: absolute; transform: translate3d(-444px, 32px, 0px); top: 0px; left: 0px; will-change: transform;">
                            @foreach(user('ap')->unreadNotifications as $notification)

                                <a href="#" class="dropdown-item d-flex">
                                    <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/male/41.jpg)"></span>
                                    <div>
                                        <strong>Notice!</strong> {{ $notification->data['data'] }}
                                        <div class="small text-muted">{{ $notification->created_at->diffForHumans() }}</div>
                                    </div>
                                </a>
                            @endforeach
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item text-center">Mark all as read</a>
                        </div>
                    </div>

                    <div class="dropdown">
                        <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown" aria-expanded="false">
                            <span class="avatar avatar-indigo">{{ strtoupper(auth('ap')->user()->firstname[0].auth('ap')->user()->lastname[0]) }}</span>
                            <span class="ml-2 d-none d-lg-block">
                      <span class="text-default">{{ ucwords(auth('ap')->user()->firstname. ' ' .auth('ap')->user()->lastname) }}</span>
                      <small class="text-muted d-block mt-1">{{ auth('ap')->user()->email }}</small>
                    </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" x-placement="bottom-end"
                             style="position: absolute; transform: translate3d(136px, 32px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <a class="dropdown-item" href="#">
                                <i class="dropdown-icon fe fe-user"></i> Profile
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="dropdown-icon fe fe-settings"></i> Settings
                            </a>
                            <a class="dropdown-item" href="#">
                                <span class="float-right"><span class="badge badge-primary">6</span></span>
                                <i class="dropdown-icon fe fe-mail"></i> Inbox
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="dropdown-icon fe fe-send"></i> Message
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">
                                <i class="dropdown-icon fe fe-help-circle"></i> Need help?
                            </a>
                            <a class="dropdown-item" href="{{route('auth.logout')}}">
                                <i class="dropdown-icon fe fe-log-out"></i> Sign out
                            </a>
                        </div>
                    </div>
                @endif
            </div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse"
               data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
            </a>
        </div>
    </div>
</div>
@if( auth('ap')->user() )
    <div class="header d-lg-flex p-0 collapse" id="headerMenuCollapse" style="">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 ml-auto">
                    <form class="input-icon my-3 my-lg-0">
                        <input type="search" class="form-control header-search" placeholder="Search…" tabindex="1">
                        <div class="input-icon-addon">
                            <i class="fe fe-search"></i>
                        </div>
                    </form>
                </div>
                <div class="col-lg order-lg-first">
                    <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                        <li class="nav-item">
                            <a href="{{ route('user.home') }}" class="nav-link active"><i class="fe fe-home"></i> Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-box"></i> Loan Overview</a>
                            <div class="dropdown-menu dropdown-menu-arrow">
                                <a href="{{route('user.requests')}}" class="dropdown-item">Loan Requests</a>
                                <a href="#" class="dropdown-item">Disbursed Loans</a>
                                <a href="#" class="dropdown-item">Loan History</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link"><i class="fe fe-image"></i> Repayments</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link"><i class="fe fe-file-text"></i> Support</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif
