@extends('layouts.front.master')

@section('page_title', "Request for a loan")

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-6 col-12">
            <instant-loan>Loading...</instant-loan>
        </div>
    </div>

@endsection
