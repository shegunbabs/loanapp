<div class="col-lg-3 order-lg-1 mb-4">
    @php $data = user('ap')->loanRequests()->latest()->first(); @endphp
    @if ($data)
        @if ( $data->statusPending() )
            <div class="alert alert-icon alert-danger alert-important" role="alert">
                <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i>
                Your loan request is pending at the moment.
            </div>
        @elseif ($data->statusOnHold())
            <div class="alert alert-icon alert-danger alert-important" role="alert">
                <i class="fe fe-minimize-2 mr-2" aria-hidden="true"></i>
                Your loan request is currently on hold at the moment.
            </div>
        @elseif ($data->statusProcessing())
            <div class="alert alert-icon alert-danger alert-important" role="alert">
                <i class="fe fe-bookmark mr-2" aria-hidden="true"></i>
                Your loan request is being processed at the moment.
            </div>
        @elseif ($data->statusRejected())
            <div class="alert alert-icon alert-danger alert-important" role="alert">
                <i class="fe fe-alert-octagon mr-2" aria-hidden="true"></i>
                Your loan request has been rejected. Kindly make another request
            </div>
        @elseif ($data->statusProcessed())
            <div class="alert alert-icon alert-success alert-important" role="alert">
                <i class="fe fe-bell mr-2" aria-hidden="true"></i>
                Your loan request has been successfully processed.
            </div>
        @endif
    @endif
    <div class="card">
        <div class="card-body">
            Note:
            <p class="mt-4">
                Disbursement will only be released into your account one you've fulfilled all the requirements
            </p>
        </div>
        <div class="card-body">
            Having trouble with your disbursements?
            <a href="#" class="btn btn-block btn-azure mt-3">Contact Us</a>
        </div>
    </div>
    {{--<a href="https://github.com/tabler/tabler" class="btn btn-block btn-primary mb-6">--}}
    {{--<i class="fe fe-github mr-2"></i>Browse source code--}}
    {{--</a>--}}
    {{--<!-- Getting started -->--}}
    {{--<div class="list-group list-group-transparent mb-0">--}}
    {{--<a href="../docs/index.html" class="list-group-item list-group-item-action active"><span class="icon mr-3"><i class="fe fe-flag"></i></span>Introduction</a>--}}
    {{--</div>--}}
    {{--<!-- Components -->--}}
    {{--<div class="list-group list-group-transparent mb-0">--}}
    {{--<a href="../docs/alerts.html" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fe fe-alert-triangle"></i></span>Alerts</a>--}}
    {{--<a href="../docs/avatars.html" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fe fe-user"></i></span>Avatars</a>--}}
    {{--<a href="../docs/buttons.html" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fe fe-plus-square"></i></span>Buttons</a>--}}
    {{--<a href="../docs/colors.html" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fe fe-feather"></i></span>Colors</a>--}}
    {{--<a href="../docs/cards.html" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fe fe-image"></i></span>Cards</a>--}}
    {{--<a href="../docs/charts.html" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fe fe-pie-chart"></i></span>Charts</a>--}}
    {{--<a href="../docs/form-components.html" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fe fe-check-square"></i></span>Form components</a>--}}
    {{--<a href="../docs/tags.html" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fe fe-tag"></i></span>Tags</a>--}}
    {{--<a href="../docs/typography.html" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fe fe-type"></i></span>Typography</a>--}}
    {{--</div>--}}
    {{--<div class="d-none d-lg-block mt-6">--}}
    {{--<a href="https://github.com/tabler/tabler/edit/dev/src/_docs/index.md" class="text-muted">Edit this page</a>--}}
    {{--</div>--}}
</div>
