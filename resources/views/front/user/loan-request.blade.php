@extends('layouts.front.master')

@section('page-header', 'Loan Requests')

@section('content')

    <div class="page-header">
        <h1 class="page-title">Loan Request</h1>
    </div>
    <div class="row">
        @include('front.user.partials.sidebar')
        <div class="col-lg-9">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">

                                    <ul style="list-style: none;" class="list-request-loan row">
                                        <li class="col-4">
                                            <span class="_label">Request Status: </span>
                                            <loan-request-status :status="{{$data->request_status}}"></loan-request-status>
                                        </li>
                                        <li class="col-4">
                                            <span class="_label">Offer Status: </span>
                                            <loan-request-offer-status :status="{{$data->offer_status}}"></loan-request-offer-status>
                                        </li>
                                        <li class="col-4">
                                            <span class="_label ">Request Date:</span>
                                            <span class="_label-data">{{ $data->created_at->format('d M Y') }}</span>
                                        </li>
                                        <li class="col-4">
                                            <span class="_label">Amount</span>
                                            <span>{{ toNaira($data->amount) }}</span>
                                        </li>
                                        <li class="col-4">
                                            <span class="_label">Tenor</span>
                                            <span>{{ $data->tenor }} Months</span>
                                        </li>
                                        <li class="col-4">
                                            <span class="_label">Interest Rate</span>
                                            <span>{{ $data->interest_rate_percent }} %</span>
                                        </li>
                                        <li class="col-4">
                                            <span class="_label">Amount Payable Monthly</span>
                                            <span>{{ toNaira($data->monthly_amount_payable) }}</span>
                                        </li>
                                        <li class="col-4">
                                            <span class="_label">Total Amount Payable</span>
                                            <span>{{ toNaira($data->total_amount_payable) }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
