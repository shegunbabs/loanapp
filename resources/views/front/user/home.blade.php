@extends('layouts.front.master')

@section('content')

    <div class="row">
        @include('front.user.partials.sidebar')
        <div class="col-lg-9">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="card p-3">
                        <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-blue mr-3">
                      <i class="fe fe-dollar-sign"></i>
                    </span>
                            <div>
                                <h4 class="m-0"><a href="javascript:void(0)">0
                                        <small>Approved Requests</small>
                                    </a></h4>
                                <small class="text-muted">1 waiting request</small>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="card p-3">
                        <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-yellow mr-3">
                      <i class="fe fe-message-square"></i>
                    </span>
                            <div>
                                <h4 class="m-0"><a href="javascript:void(0)">0 <small>Loan Applications</small></a></h4>
                                <small class="text-muted">1 waiting</small>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="card p-3">
                        <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-green mr-3">
                      <i class="fe fe-shopping-cart"></i>
                    </span>
                            <div>
                                <h4 class="m-0"><a href="javascript:void(0)">0 <small>Disbursed Loan</small></a></h4>
                                {{--<small class="text-muted">32 shipped</small>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="text-wrap p-lg-6">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
