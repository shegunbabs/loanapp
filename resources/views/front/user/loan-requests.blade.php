@extends('layouts.front.master')

@section('content')

    <div class="page-header">
        <h1 class="page-title">Loan Requests</h1>
    </div>
    <div class="row">
        @include('front.user.partials.sidebar')
        <div class="col-lg-9">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if ($data->count())
                            <div class="table-responsive" style="min-height:400px;">
                                <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Tenor</th>
                                        <th>Monthly Repayment</th>
                                        <th>Total Repayment</th>
                                        <th>Request Status</th>
                                        <th>Offer Status</th>
                                        <th class="text-center"><i class="icon-settings"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $counter = 0; @endphp
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{ ++$counter }}</td>
                                            <td>{{ $row->created_at->format("d M Y") }}</td>
                                            <td>{{ toNaira($row->amount) }}</td>
                                            <td>{{ $row->tenor }} {{ $row->tenor > 1 ? ' Months' : ' Month' }}</td>
                                            <td>{{ toNaira($emi = emi(7/100, $row->tenor, $row->amount)) }}</td>
                                            <td>{{ toNaira($emi * $row->tenor) }}</td>
                                            <td>
                                                <loan-request-status :status="{{$row->request_status}}"></loan-request-status>
                                            </td>
                                            <td>
                                                <loan-request-offer-status :status="{{$row->offer_status}}"></loan-request-offer-status>
                                            </td>
                                            <td class="text-center">
                                                <div class="item-action dropdown">
                                                    <a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-more-vertical"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a href="{{route('user.request',['requestId'=>$row->id])}}" class="dropdown-item"><i class="dropdown-icon fe fe-tag"></i> View </a>
                                                        <div class="dropdown-divider"></div>
                                                        <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-link"></i> Withdraw request</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="card-body">
                                <div class="alert alert-info">
                                    No existing loan request
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
