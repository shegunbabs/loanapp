@extends('layouts.front.auth')

@section('content')

    <form class="card" action="{{route('auth.post.login')}}" method="post">
        @csrf
        <div class="card-body p-6">
            <div class="card-title">Login to your account</div>
            <div class="form-group">
                <label class="form-label">Email address</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{old('email')}}" name="email"
                       id="email" aria-describedby="emailHelp" placeholder="Enter email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label class="form-label">
                    Password
                    <a href="#" class="float-right small">I forgot password</a>
                </label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password" name="password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                    <span class="custom-control-label">Remember me</span>
                </label>
            </div>
            <div class="form-footer">
                <button type="submit" class="btn btn-primary btn-block">Sign in</button>
            </div>
        </div>
    </form>

@endsection
