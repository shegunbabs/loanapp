<div id="flash-overlay-modal" class="modal fade {{ $modalClass or '' }}" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header py-2 px-4">
                <h5 class="modal-title">{{ $title }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <p>{!! $body !!}</p>
            </div>
            <div class="modal-footer py-1 px-4">
                <button type="button" class="btn btn-gray" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
