@extends('layouts.backoffice.master')

@section('page-header', 'Acyivated Standing Orders')

@section('content')
    <div class="card">
        <div class="card-header">Activated Standing Orders</div>
        <div class="card-body">
            @if( $data->count() )
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Payers Name</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Total Amount</th>
                        <th scope="col">Start Date</th>
                        <th scope="col">Payment Ref</th>
                        <th scope="col">Payment Confirmed</th>
                        <th scope="col">Finished</th>
                        <th scope="col">A</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $counter = 1; ?>
                    @foreach( $data as $row )
                        <tr>
                            <th scope="row">{{ $counter++ }}</th>
                            <td>{{ ucwords($row->payers_name) }}</td>
                            <td>{{ toNaira($row->total_amount) }}</td>
                            <td>{{ toNaira($row->amount) }}</td>
                            <td>{{ $row->start_date->format('d/m/Y') }}</td>
                            <td>{{ $row->payment_reference }}</td>
                            <td>{{ isset($payment_confirmed) === TRUE ? 'Co' : 'Nc' }}</td>
                            <td>{{ isset($finished) === TRUE ? 'Fin' : 'NFin'}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-info">
                    No running/activated standing orders yet
                </div>
            @endif
        </div>
    </div>
@endsection
