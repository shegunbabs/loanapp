@extends('layouts.backoffice.master')

@section('page-header', 'View Standing Order')

@section('content')

    <div class="row">

        <div class="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">

            <div class="card">
                <div class="card-body">
                    <div class="user-avatar text-center d-block">
                        <img src="assets/images/avatar-1.jpg" alt="User Avatar" class="rounded-circle user-avatar-xxl">
                    </div>
                    <div class="text-center">
                        <h2 class="font-24 mb-0">{{ ucwords($data->payers_name) }}</h2>
                        {{--<p>Project Manager @Influnce</p>--}}
                    </div>
                </div>
                <div class="card-body border-top">
                    <h3 class="font-16">Contact Information</h3>
                    <div class="">
                        <ul class="list-unstyled mb-0">
                            <li class="mb-2"><i class="fas fa-fw fa-envelope mr-2"></i>{{ $data->payers_email }}</li>
                            <li class="mb-0"><i class="fas fa-fw fa-phone mr-2"></i>{{ $data->payers_phone }}</li>
                        </ul>
                    </div>
                </div>
                <div class="card-body border-top">
                    <h3 class="font-16">Transaction Information</h3>
                    <div class="">
                        <ul class="list-unstyled mb-0">
                            @if ($trans)
                                <li class="mb-2">Gateway response: {{ $trans->gateway_response }}</li>
                                <li class="mb-0">Amount charegd: {{ toNaira($trans->amount) }}</li>
                            @endif
                        </ul>
                    </div>
                </div>

                <div class="card-body border-top">
                    @if($trans)
                        <h3 class="font-16">Card Details ({{$trans->authorization->brand}})</h3>
                        <h2 class="mb-0">**** **** **** {{$trans->authorization->last4}}</h2>
                        <h4 class="mb-0">exp: {{ $trans->authorization->exp_month. '/' .$trans->authorization->exp_year }}</h4>
                        <h5 class="mb-0">{{ $trans->authorization->authorization_code }}</h5>
                    @else
                        <div class="alert alert-info alert-important">
                            No transaction record
                        </div>
                    @endif
                </div>
            </div>

        </div>


        <div class="col-xl-9 col-lg-9 col-md-7 col-sm-12 col-12">

            <h3 class="section-title">Standing Order Details</h3>

            <div class="card">
                <div class="card-body">
                    <div class="m-4 p-0 list-none flex so-list">

                        <div class="li">
                            <label>Beneficiary</label>
                            <div>{{ $data->beneficiary }}</div>
                        </div>

                        <div class="li">
                            <label>Purpose</label>
                            <div>{{ $data->purpose }}</div>
                        </div>

                        <div class="li">
                            <label>Payment Frequency</label>
                            <div>{{ $data->payment_frequency }}</div>
                        </div>


                        <div class="li">
                            <label>Actual Amount</label>
                            <div>{{ toNaira($data->amount) }}</div>
                        </div>

                        <div class="li">
                            <label>Transaction Amount</label>
                            <div>{{ toNaira($data->total_amount) }}</div>
                        </div>

                        <div class="li">
                            <label>Start Date</label>
                            <div>{{ $data->start_date->format('d M, Y') }}</div>
                        </div>

                        <div class="li">
                            <label>End Date</label>
                            <div>{{ $data->end_date->format('d M, Y') }}</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <h5 class="card-header">Transaction data</h5>
                <div class="card-body p-0">
                    @if($trans)
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="bg-light">
                                <tr class="border-0">
                                    <th class="border-0">Domain</th>
                                    <th class="border-0">Status</th>
                                    <th class="border-0">Payment Ref</th>
                                    <th class="border-0">Amount</th>
                                    <th class="border-0">Message</th>
                                    <th class="border-0">Gateway Response</th>
                                    <th class="border-0">IP Address</th>
                                    <th class="border-0">Transaction Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $trans->domain }}</td>
                                    <td>{{ $trans->status }}</td>
                                    <td>{{ $trans->reference }}</td>
                                    <td>{{ toNaira($trans->amount) }}</td>
                                    <td>{{ $trans->message }}</td>
                                    <td>{{ $trans->gateway_response }}</td>
                                    <td>{{ $trans->ip_address }}</td>
                                    <td>{{ $trans->transaction_date->format('d M, Y') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-info alert-important">
                            No transaction record
                        </div>
                    @endif
                </div>
            </div>

            <div class="card">
                <h5 class="card-header">Repayment Breakdown</h5>
                <div class="card-body p-0">
                    @if($data->standingOrderRepayment->count())
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="bg-light">
                                <tr class="border-0">
                                    <th class="border-0">Payment Date</th>
                                    <th class="border-0">Amount</th>
                                    <th class="border-0">Transaction Amount</th>
                                    <th class="border-0">Payment Confirmed</th>
                                    <th class="border-0">Payment Ref</th>
                                    <th class="border-0">Payment Status</th>
                                    <th class="border-0">Auth Check Status</th>
                                    <th class="border-0">Auth Check Msg</th>
                                    <th class="border-0">Auth Check Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data->standingOrderRepayment as $repaymentItem)
                                    <tr class="{{ !$repaymentItem->payment_confirmed ? 'text-red-500' : 'text-green-500' }}">
                                        <td>{{ $repaymentItem->transaction_date->format('d M, Y') }}</td>
                                        <td>{{ toNaira($repaymentItem->amount) }}</td>
                                        <td>{{ toNaira($repaymentItem->transaction_amount) }}</td>
                                        <td>{{ $repaymentItem->payment_confirmed ? 'confirmed' : 'not confirmed' }}</td>
                                        <td>{{ $repaymentItem->payment_reference }}</td>
                                        <td>{{ !$repaymentItem->processed ? 'awaiting payment' : 'payment processed' }}</td>
                                        <td>{{ isset($repaymentItem->authorization_check_status) === true ? $repaymentItem->authorization_check_status === 0 ? 'failed' : 'success'  : 'not checked' }}</td>
                                        <td>{{ $repaymentItem->authorization_check_message }}</td>
                                        <td>{{ $repaymentItem->authorization_check_last_timestamp ? $repaymentItem->authorization_check_last_timestamp->format("d M, Y - H:i") : '' }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-info alert-important">
                            Transaction authorization for this customer is not reusable.
                        </div>
                    @endif
                </div>
            </div>

        </div>

    </div>

@endsection
