@extends('layouts.backoffice.master')

@section('page-header', 'Pending Standing Orders')

@section('content')
    <div class="card">
        <div class="card-header">Pending Standing Orders</div>
        <div class="card-body" style="min-height:400px;">
            @if( $data->count() )
                <table class="table table-hover table-responsive" style="min-height: 400px">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Payers Name</th>
                        <th scope="col">Payers Email</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Total Amount</th>
                        <th scope="col">Start Date</th>
                        <th scope="col">End Date</th>
                        <th scope="col">Payment Ref</th>
                        <th scope="col">Payment Confirmed</th>
                        <th scope="col">Valid Auth?</th>
                        <th scope="col">Activated?</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $counter = 1; ?>
                    @foreach( $data as $row )
                        <tr>
                            <th scope="row">{{ $counter++ }}</th>
                            <td>{{ ucwords($row->payers_name) }}</td>
                            <td>{{ ucwords($row->payers_email) }}</td>
                            <td>{{ toNaira($row->amount) }}</td>
                            <td>{{ toNaira($row->total_amount) }}</td>
                            <td>{{ $row->start_date->format('d/m/Y') }}</td>
                            <td>{{ $row->end_date->format('d/m/Y') }}</td>
                            <td>{{ $row->payment_reference }}</td>
                            <td>{!! isset($row->payment_confirmed) === TRUE ? 'Confirmed' : 'Nc <a href="#" class="btn btn-xs btn-light">Confirm</a>' !!}</td>
                            <td>{{ $row->authorization_reusable ? 'Yes' : 'No' }}</td>
                            <td>{{ $row->activated ? 'Yes' : 'No' }}</td>
                            <td>
                                <div class="dropright">
                                    <a class="py-2 px-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="{{route('so.pending.view', ['payment_ref' => $row->payment_reference])}}">View</a>
                                        <a class="dropdown-item" href="#">Activate</a>
                                        <a class="dropdown-item" href="#">Disable</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-info alert-important">
                    No pending standing orders yet
                </div>
            @endif
        </div>
    </div>
@endsection
