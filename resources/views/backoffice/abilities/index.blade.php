@extends('layouts.backoffice.master')

@section('page-header', 'Abilities')

@section('content')

    <div class="row">

        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">Create Ability</div>
                <form action="{{route('abilities.store')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name')}}" placeholder="Ability Name">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-md-8">
                                <div class="form-group">
                                    <label for="name">Title</label>
                                    <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{old('title')}}" placeholder="Ability Title">
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body border-top">
                        <div class="row">
                            <div class="col-12 div col-md-12">
                                <button class="btn btn-primary float-right">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="card">
                <h5 class="card-header">Roles</h5>
                <div class="card-body p-0">
                    @if($data->count())
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="bg-light">
                                <tr class="border-0">
                                    <th class="border-0">#</th>
                                    <th class="border-0">Role Name</th>
                                    <th class="border-0">Role Title</th>
                                    <th class="border-0">Status</th>
                                    <th class="border-0">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $counter=0;
                                @endphp
                                @foreach($data as $row)
                                    <tr>
                                        <td> {{ ++$counter }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->title }}</td>
                                        <td><span class="badge-dot badge-success mr-1"></span>active</td>
                                        <td>
                                            <div class="dropdown">
                                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                    <i class="mdi mdi-dots-vertical"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"
                                                     style="position: absolute; transform: translate3d(-146px, 19px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                    <a href="javascript:void(0);" class="dropdown-item">Abilities</a>
                                                    <a href="#" class="dropdown-item"
                                                       onclick="event.preventDefault(); document.getElementById('delete-form-{{$row->name}}').submit();">
                                                        Delete
                                                    </a>
                                                    <form id="delete-form-{{$row->name}}" action="{{ route('roles.destroy', ['role'=>$row->name]) }}" method="POST" style="display: none;">
                                                        @csrf @method('DELETE')
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="9">&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-info my-3 mx-4 alert-important">
                            There are no roles yet.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>



@endsection
