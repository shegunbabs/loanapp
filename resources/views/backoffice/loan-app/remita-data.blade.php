@extends('layouts.backoffice.master')

@section('page-header', 'Pull Remita Data')

@section('content')

    <div class="row">
        <div class="col-12">
            <remita-salary-history></remita-salary-history>
        </div>
    </div>

@endsection
