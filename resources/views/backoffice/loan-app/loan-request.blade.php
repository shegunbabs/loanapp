@extends('layouts.backoffice.master')

@section('page-header', 'Loan Request')

@section('content')

    <div class="row">
        <div class="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="user-avatar text-center d-block">
                        <img src="assets/images/avatar-1.jpg" alt="User Avatar" class="rounded-circle user-avatar-xxl">
                    </div>
                    <div class="text-center">
                        <h2 class="font-24 mb-0">{{ ucwords($data->applicant->firstname. ' ' .$data->applicant->lastname) }}</h2>
                        {{--<p>Project Manager @Influnce</p>--}}
                    </div>
                </div>
                <div class="card-body border-top">
                    <h3 class="font-16">Contact Information</h3>
                    <div class="">
                        <ul class="list-unstyled mb-0">
                            <li class="mb-2"><i class="fas fa-fw fa-envelope mr-2"></i>{{ $data->applicant->email }}</li>
                            <li class="mb-0"><i class="fas fa-fw fa-phone mr-2"></i>{{ $data->applicant->mobile }}</li>
                        </ul>
                    </div>
                </div>
                <div class="card-body border-top">
                    <h3 class="font-16">Status</h3>
                    <h1 class="mb-0">{{ $data->statusText() }}</h1><h5>{{$data->offerStatus()}}</h5>
                </div>
                <div class="card-body border-top">
                    @if ($data->applicant->isNotRemitaCustomer() && !$data->statusRejected())
                        <a class="btn btn-warning m-2" onclick="javascript: return confirm('Are you sure')"
                           href="{{route('bo.loan.show', ['id' => $data->id, 'rejectRequest'=>str_random()])}}">Reject Request</a>
                    @else

                        @if($data->statusPending())
                            <button class="btn-primary display-block m-2">Put Request On-Hold</button>
                            <button class="btn-primary display-block m-2">Set Request Processing</button>
                            <button class="btn-warning display-block m-2">Reject Request</button>
                        @endif

                        @if ( $data->statusOnHold() )
                            <button class="btn-primary display-block m-2">Set Request Processing</button>
                            <button class="btn-warning display-block m-2">Reject Request</button>
                        @endif

                        @if( $data->statusProcessing() )
                            <button class="btn-warning display-block m-2">Reject Request</button>
                            <button class="btn-success display-block m-2">Give Offer</button>
                        @endif

                        @if( $data->statusRejected() )
                            <span>Request Truncated.</span>
                        @endif


                    @endif

                </div>

            </div>
        </div>

        <div class="col-xl-9 col-lg-9 col-md-7 col-sm-12 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <h3>Request Details</h3>
                            <ul style="list-style: none;" class="list-request-loan">
                                <li>
                                    <span class="_label">Request Date</span>
                                    <span class="_label-data">{{ $data->created_at->format('d M Y') }}</span>
                                </li>
                                <li>
                                    <span class="_label">Amount</span>
                                    <span>{{ toNaira($data->amount) }}</span>
                                </li>
                                <li>
                                    <span class="_label">Tenor</span>
                                    <span>{{ $data->tenor }}</span>
                                </li>
                                <li>
                                    <span class="_label">Interest Rate</span>
                                    <span>{{ $data->interest_rate_percent }} %</span>
                                </li>
                                <li>
                                    <span class="_label">Amount Payable Monthly</span>
                                    <span>{{ toNaira($data->monthly_amount_payable) }}</span>
                                </li>
                                <li>
                                    <span class="_label">Total Amount Payable</span>
                                    <span>{{ toNaira($data->total_amount_payable) }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 border-left">
                            <h3>Remita Salary Details</h3>
                            @if ($data->applicant->hasRemitaData())
                                <ul class="list-request-loan">
                                    <li>
                                        <span class="_label">Customer ID</span>
                                        <span class="_label-data">{{ $data->applicant->remitaData->customer_id }}</span>
                                    </li>
                                    <li>
                                        <span class="_label">Account Number</span>
                                        <span class="_label-data">{{ $data->applicant->remitaData->account_number }}</span>
                                    </li>
                                    <li>
                                        <span class="_label">Bank Name</span>
                                    </li>
                                    <li>
                                        <span class="_label">Company Name</span>
                                        <span class="_label-data">{{ $data->applicant->remitaData->company_name }}</span>
                                    </li>
                                    <li>
                                        <span class="_label">First Payment Date</span>
                                        <span class="_label-data">{{ $data->applicant->remitaData->first_payment_date->format("d M Y") }}</span>
                                    </li>
                                    <li>
                                        <span class="_label">Salary Amount / Average Salary</span>
                                        <span class="_label-data">{{ toNaira($data->applicant->remitaData->remitaSalaryPayments->first()->amount). " / "
                                        .toNaira($data->applicant->remitaData->remitaSalaryPayments->avg('amount')) }}</span>
                                    </li>
                                </ul>
                            @else

                                <div class="alert alert-info alert-important">
                                    This customer has no remita data.
                                </div>

                                @if( $data->applicant->hasRemitaSalaryResponseData() )
                                    <div class="alert alert-warning alert-important">
                                        This customer does not exist on Remita <br>
                                        [<a href="{{route('bo.loan.show', ['id' => $data->id, 'checkRemitaData'=>str_random()])}}" class="">Force Check for Remita Data</a>]
                                    </div>
                                @else
                                    <a href="{{route('bo.loan.show', ['id' => $data->id, 'checkRemitaData'=>str_random()])}}" class="btn btn-sm btn-primary">Check for Remita Data</a>
                                @endif

                            @endif

                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-light">
                            <tr class="border-0">
                                <th class="border-0">#</th>
                                <th class="border-0">Date</th>
                                <th class="border-0">Principal Amount</th>
                                <th class="border-0">Interest Amount</th>
                                <th class="border-0">Total Amount Payable</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $counter = 0 @endphp
                            @foreach($lrs['data'] as $item)
                                <tr>
                                    <td>{{ ++$counter }}</td>
                                    <td> {{ $item['due_date'] }} </td>
                                    <td> {{ toNaira($item['principal']) }} </td>
                                    <td>{{ toNaira($item['interest']) }}</td>
                                    <td>{{ toNaira($item['total']) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- ============================================================== -->
    <!-- end campaign tab one -->
    <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end campaign data -->
    <!-- ============================================================== -->
    </div>

@endsection
