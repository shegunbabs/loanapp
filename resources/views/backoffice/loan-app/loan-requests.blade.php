@extends('layouts.backoffice.master')

@section('page-header', 'Loan Requests')

@section('content')

    <div class="card">
        <h5 class="card-header">Loan Requests</h5>
        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table">
                    <thead class="bg-light">
                    <tr class="border-0">
                        <th class="border-0">#</th>
                        <th class="border-0">Date</th>
                        <th class="border-0">Customer Name</th>
                        <th class="border-0">Loan Amount</th>
                        <th class="border-0">Loan Tenor/Interest Rate</th>
                        <th class="border-0">Monthly Repayment</th>
                        <th class="border-0">Total Repayment</th>
                        <th class="border-0">Request Status</th>
                        <th class="border-0">Offer Status</th>
                        <th class="text-center"><i class="icon-settings"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $counter=0; @endphp
                    @foreach($data as $row)
                        <tr>
                            <td>{{ ++$counter }}</td>
                            <td>{{ $row->created_at->format('d M Y') }}</td>
                            <td>{{ ucwords($row->applicant->firstname. ' ' .$row->applicant->lastname) }}</td>
                            <td>{{ toNaira($row->amount) }}</td>
                            <td>{{ $row->tenor. ' Months' . ' / ' .$row->interest_rate_percent. '%' }}</td>
                            <td>{{ toNaira($row->monthly_amount_payable) }}</td>
                            <td>{{ toNaira($row->total_amount_payable) }}</td>
                            <td>
                                <loan-request-status :status="{{$row->request_status}}"></loan-request-status>
                            </td>
                            <td>
                                <loan-request-offer-status :status="{{$row->offer_status}}"></loan-request-offer-status>
                            </td>
                            <td>
                            <td class="text-center">
                                <div class="item-action dropdown">
                                    <a href="javascript:void(0)" data-toggle="dropdown" class="icon">
                                        <i class="fe fe-more-vertical"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{route('bo.loan.show',['id'=>$row->id])}}" class="dropdown-item"> <i class="dropdown-icon fe fe-tag"></i> View </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="javascript:void(0)" class="dropdown-item">
                                            <i class="dropdown-icon fe fe-link"></i> Withdraw request</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                    <tr>
                        <td colspan="10">
                            <select name="request_status" id="request_status" class="form-control float-right" style="width:200px;">
                                <option value="">Choose Request Status</option>
                                <option value="">All</option>
                                <option value="pending">Pending</option>
                                <option value="on-hold">On-Hold</option>
                                <option value="processing">Processing</option>
                                <option value="rejected">Rejected</option>
                                <option value="processed">Processed</option>
                                <option value="withdrawn">Withdrawn</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        let $select = document.getElementById('request_status');
        var $url = "{{ route('bo.loan.requests') }}";
        $select.addEventListener('change', function(){
            let $option = this.options[this.selectedIndex].value
            if ($option) {
                $link = $url + '?status=' + $option;
            }else {
                $link = $url;
            }
           window.location.href = $link
        });
    </script>
@endpush
