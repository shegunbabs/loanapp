@extends('layouts.backoffice.master')

@section('page-header', 'All Users')

@section('content')

    <div class="card">
        <h5 class="card-header">Users</h5>
        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table">
                    <thead class="bg-light">
                    <tr class="border-0">
                        <th class="border-0">#</th>
                        <th class="border-0">Name</th>
                        <th class="border-0">Email</th>
                        <th class="border-0">Role</th>
                        <th class="border-0">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $counter=0; @endphp
                    @foreach($users as $user)
                    <tr>
                        <td>{{ ++$counter }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->getRoles() }}</td>
                        <td>
                            <a href="{{route('users.show', ['user'=>$user->id])}}" class="btn btn-xs">View</a>
                        </td>

                    </tr>
                    @endforeach

                    <tr>
                        <td colspan="9"><a href="#" class="btn btn-outline-light float-right">View Details</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
