@extends('layouts.backoffice.master')

@section('page-header', "View User")

@section('content')

    <div class="card">
        <h5 class="card-header">Users</h5>
        <div class="card-body p-0">

            <ul class="list-group list-group-flush">

                <li class="list-group-item ">
                    <span class="traffic-sales-name">Name</span>
                    <span class="traffic-sales-amount">{{$user->name}}
                    </span>
                </li>

                <li class="list-group-item ">
                    <span class="traffic-sales-name">Email</span>
                    <span class="traffic-sales-amount">{{$user->email}}
                    </span>
                </li>

                <li class="list-group-item ">
                    <span class="traffic-sales-name">Roles</span>
                    @if ($roles)
                        <ul class="list-group list-group-flush">
                            <form>
                                @foreach($roles as $role)
                                    <li class="list-group-item">
                                        <label class="custom-control custom-checkbox">
                                            <input id="role-{{$role->name}}" name="roles[]" type="checkbox" value="{{$role->name}}" class="custom-control-input"
                                                {{ in_array($role, $user->getRoles()->toArray()) ? 'checked' : '' }}>
                                            <span class="custom-control-label">{{$role->title}}</span>
                                        </label>
                                    </li>
                                @endforeach
                                <button class="btn btn-xs btn-primary">Save</button>
                            </form>
                        </ul>
                    @endif
                </li>


            </ul>
        </div>
    </div>

@endsection
