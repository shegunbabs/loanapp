@extends('layouts.backoffice.master')

@section('page-header', 'Assign Abilities to Roles')

@section('content')

    <div class="col-12 col-md-12">

        <div class="card card-fluid">

            <div class="card-header">Assign Permissions to Roles</div>

            <ul class="list-group list-group-bordered list-group-flush">
                @foreach($roles as $role)
                    <?php $roleAbilities = $role->getAbilities()->pluck('name')->toArray(); ?>
                    <li class="list-group-item">
                        <form method="post" action="{{route('assign.create')}}">
                            @csrf
                            <input type="hidden" name="role" value="{{$role->name}}">
                            {{ ucwords($role->name) }} ({{ ucwords($role->title) }})

                            <ul style="list-style: none;">
                                @foreach($abilities as $ability)
                                    <li>
                                        <label class="custom-control custom-checkbox">
                                            <input id="ability-{{$ability->name}}" name="ability[]" type="checkbox" value="{{$ability->name}}" class="custom-control-input"
                                                   {{ in_array($ability->name, $roleAbilities) ? 'checked' : '' }}>
                                            <span class="custom-control-label">{{$ability->name}}</span>
                                        </label>
                                    </li>
                                @endforeach
                                <button class="btn btn-primary btn-xs" type="submit">Save</button>
                            </ul>
                        </form>
                    </li>
                @endforeach
            </ul>

        </div>

    </div>

@endsection
