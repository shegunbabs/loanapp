<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

Route::prefix('back-office')->namespace('BackOffice')->group(function () {
    Auth::routes();

    Route::get('', function () {
        return view('layouts.backoffice.master');
    })->middleware('auth')->name('bo.home');

    Route::prefix('standing-orders')->namespace('SO')->middleware('auth')->name('so.')->group(function () {
        Route::get('pending', 'SoController@pending')->name('pending');
        Route::get('pending/{payment_ref}/view', 'SoController@view')->name('pending.view');
        Route::get('activated', 'SoController@activated')->name('activated');
        Route::get('invalid', 'SoController@invalid')->name('invalid');
    });

    Route::prefix('roles')->namespace('Roles')->middleware('auth')->group(function () {
        Route::get('/', 'RoleController@index')->name('roles.index');
        Route::post('/', 'RoleController@store')->name('roles.store');
        Route::delete('/{role}', 'RoleController@destroy')->name('roles.destroy');

        Route::get('assign', 'AssignRolesToPermsController@index')->name('roles.assign');
        Route::post('assign', 'AssignRolesToPermsController@store')->name('assign.create');
    });
    Route::prefix('abilities')->namespace('Roles')->middleware('auth')->group(function () {
        Route::get('/', 'AbilityController@index')->name('abilities.index');
        Route::post('/', 'AbilityController@store')->name('abilities.store');
        Route::delete('/{ability}', 'AbilityController@destroy')->name('abilities.destroy');
    });
    Route::prefix('users')->middleware('auth')->group(function () {
        Route::get('/', 'UserController@index')->name('users.index');
        Route::get('show/{user}', 'UserController@show')->name('users.show');
    });

    Route::prefix('loan-app')->middleware('auth')->namespace('LoanApp')->name('bo.loan.')->group(function () {
        Route::get('loan-requests', 'LoanRequestController@index')->name('requests');
        Route::get('{id}/loan-request', 'LoanRequestController@show')->name('show');

        Route::get('remita-salary-history', 'LoanRequestController@remitaSalaryHistory');
    });


    Route::get('test', function (\App\Library\SalaryHistoryWrapper $wrapper, \App\Library\DoScoring $doScoring) {

        dd(
            \App\Models\Transaction::first()->standingOrder->first(),
            \App\Models\Transaction::first()->Customer->first(),
            \App\Models\Transaction::first()->authorization()->first()
        );


        //echo toNaira(roundNearestHundredUp(pv(7/100, 6, 21000)));
    });


});
Route::get('start-loan-request', function () {
    return view('front.loan-request.start');
})->name('start.request');
Route::prefix('auth')->namespace('Front\Auth')->group(function () {
    Route::get('login', 'LoginController@showLoginForm')->name('auth.login');
    Route::post('login', 'LoginController@login')->name('auth.post.login');
    Route::post('logout', 'LoginController@logout');
    Route::get('logout', 'LoginController@logout')->name('auth.logout');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm');
    Route::post('password/reset', 'ResetPasswordController@reset');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm');
    Route::post('register', 'RegisterController@register');
    Route::get('register', 'RegisterController@showRegistrationForm');

    Route::get('home', function () {
        return 'Auth Home';
    })->middleware('ap.auth:ap')->name('auth.home');
});

Route::prefix('user')->namespace('Front')->middleware('ap.auth:ap')->name('user.')->group(function () {
    Route::get('home', 'AppController@home')->name('home');
    Route::get('loan-requests', 'AppController@loanRequests')->name('requests');
    Route::get('/loan-request/{requestId}/view', 'AppController@showLoanRequest')->name('request');
});


Route::get('/', function () {
    return view('welcome');
});

Route::prefix('standing-order')->namespace('StandingOrder')->group(function () {

    Route::get('', 'StandingOrderController@create')->name('so.create');
    Route::get('/', 'StandingOrderController@create')->name('so.create');
    Route::get('create', 'StandingOrderController@create')->name('so.create');
    Route::get('verify-initial-charge', 'StandingOrderController@verify')->name('so.verify.reference');

    Route::get('test', 'StandingOrderController@test');
});

Route::get('backoffice/standing-order', function () {

    $p = new ShegunBabs\PayStack\PayStack(env('PAYSTACK_SECRET_KEY'));
    $response = $p->transaction->initialize(['email' => 'shegun.babs@gmail.com', 'amount' => 1000 * 100, 'callback_url' => 'http://loanapp.local/standing-order/verify-initial-charge']);
    $url = $response['response']->data->authorization_url;
    dd($url);

    //ShegunBabs\PayStack\call_url($url);

    //return view('bo.standing-order');
});

Route::get('standing-order/verify-initial-charges', function (Request $request) {

    $trxref = $request->query('trxref');
    $ref = $request->query('reference');

    if (empty($ref)) {
        return view('bo.so.noref');
    }


    $p = new \ShegunBabs\PayStack\PayStack(env('PAYSTACK_SECRET_KEY'));
    $res = $p->transaction->verify($ref);
    Log::info($res['response']);
    dd($res['response']);
});

Route::get('privacy-policy', 'FrontOffice\WebController@privacy');

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('test')->group(function () {

    Route::get('sms', function(){
        $res = app('Infobip')->sms->single([

        ]);
    });

    Route::get('mail', function () {
        $loanRequest = \App\Models\App\LoanRequest::first();
        //return (new \App\Notifications\LoanRequestRejected($loanRequest))->toMail($loanRequest->applicant);
        //dd($loanRequest->applicant()->first());
        Notification::send($loanRequest->applicant()->first(), new \App\Notifications\App\LoanRequestRejected($loanRequest));
    });

});
Route::get('misc/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
