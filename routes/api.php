<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function(){

    Route::prefix('remita')->group(function ()
    {
        Route::post('salary-history', 'API\RemitaController@getSalaryHistory');
    });


    Route::prefix('standing-order')->group(function ()
    {
        Route::post('store', 'StandingOrder\ApiController@store');
    });

    Route::prefix('authorization')->namespace('API')->group(function()
    {
        Route::post('check-authorization',  'AuthorizationController@checkAuth');
        Route::post('charge-authorization', 'AuthorizationController@chargeAuth');
    });



    Route::post('start-loan-request',       'API\StartLoanRequestController@index');
    Route::post('start-loan-request-II',    'API\StartLoanRequestController@stepII');
    Route::post('start-loan-resend-otp',    'API\StartLoanRequestController@resendOtp');
    Route::post('start-loan-submit-otp',    'API\StartLoanRequestController@submitOtp');
    Route::post('start-loan-finish',        'API\StartLoanRequestController@finish');

});
