<?php

$merchantId = env('REMITA_MERCHANT_ID');
$apiKey = env('REMITA_API_KEY');
$apiToken = env('REMITA_API_TOKEN');
$requestId = round(microtime(true) * 1000);
$randomNumber = mt_rand(100000, 999999);
$authorizationCode = $randomNumber;
$apiHash = hash("sha512", $apiKey.$requestId.$apiToken);
$authorization = "remitaConsumerKey=$apiKey, remitaConsumerToken=$apiHash";

return [

    'base_url' => env('REMITA_BASE_URL'),

    'merchant_id' => $merchantId,

    'api_key' => $apiKey,

    'api_token' => $apiToken,

    'request_id' => $requestId,

    'authorisation_code' => $authorizationCode,

    'authorization' => $authorization,

    'random_number' => $randomNumber,

    'salary_history_uri' => env('SALARY_HISTORY_URL')

];
