<?php
/**
 * Created by PhpStorm.
 * User: shegun
 * Date: 6/7/2019
 * Time: 10:57 AM
 */
return [

    /*
    |--------------------------------------------------------------------------
    | PayStack Secret Key
    |--------------------------------------------------------------------------
   */

    'secret_key' => env('PAYSTACK_SECRET_KEY'),


    /*
    |--------------------------------------------------------------------------
    | PayStack Secret Key
    |--------------------------------------------------------------------------
   */

    'public_key' => env('PAYSTACK_PUBLIC_KEY'),


    /*
    |--------------------------------------------------------------------------
    | PayStack Secret Key
    |--------------------------------------------------------------------------
   */

    'base_url' => env('PAYSTACK_BASE_URL'),


    /*
    |--------------------------------------------------------------------------
    | PayStack Transaction fee for value below N2500
    |--------------------------------------------------------------------------
   */

    'transaction_fee' => 0.015,


    /*
    |--------------------------------------------------------------------------
    | PayStack Transaction fee for value below N2500
    |--------------------------------------------------------------------------
    */

    'flat_fee' => 100,

];
